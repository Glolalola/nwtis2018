/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.zrna;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.ServletContext;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.konfiguracije.bp.BP_Konfiguracija;
import org.foi.nwtis.globabic.web.podaci.Dnevnik;
import org.foi.nwtis.globabic.web.slusaci.SlusacAplikacije;

/**
 *
 * @author Gloria Babić
 */
@ManagedBean
@SessionScoped
public class PregledDnevnika {

    private ServletContext sc = null;
    private Konfiguracija konf;
    private String url;
    private String korisnikBaze;
    private String lozinkaBaze;
    private String driver;

    private List<Dnevnik> zapisi = new ArrayList<>();

    private int brojZapisaKojiSeMoguPrikazati;
    private int brojPocetnogZapisa;
    private int brojZadnjegZapisa;
    private int prvi = 0;
    private int korak;

    private boolean nemaZapisa = true;
    private boolean sljedeci = true, prethodni = true;

    private int idZapisa;
    private Date odVremena;
    private Date doVremena;
    private String adresaZahtjeva;
    private String korisnikZahtjeva;

    /**
     * Creates a new instance of PregledDnevnika
     */
    public PregledDnevnika() {
    }

    @PostConstruct
    public void init() {
        inicijaliziraj();
        dohvatiZapise();
    }

    private void inicijaliziraj() {
        sc = SlusacAplikacije.getServletContext();
        konf = (Konfiguracija) sc.getAttribute("konfiguracija");

        BP_Konfiguracija bpk = (BP_Konfiguracija) sc.getAttribute("BP_Konfig");
        if (bpk == null) {
            System.err.println("Problem s konfiguracijom!");
            return;
        }
        url = bpk.getServerDatabase() + bpk.getAdminDatabase();
        korisnikBaze = bpk.getAdminUsername();
        lozinkaBaze = bpk.getAdminPassword();
        driver = bpk.getDriverDatabase();
        brojZapisaKojiSeMoguPrikazati = Integer.parseInt(konf.dajPostavku("brojPodatakaNastranici"));

        prvi = 0;
        prethodni = false;
    }

    private void dohvatiZapise() {
        zapisi.clear();
        if (prvi == 0) {
            dohvatiZapiseizBaze(brojZapisaKojiSeMoguPrikazati, 0);
            prvi++;
        } else {
            dohvatiZapiseizBaze(brojZapisaKojiSeMoguPrikazati, korak);
            if (zapisi.isEmpty()) {
                sljedeci = false;
            }
        }
    }

    private void dohvatiZapiseizBaze(int granica, int pomak) {
        String upit = "SELECT * FROM dnevnik LIMIT ? OFFSET ?";
        // String upit = sastaviUpit(granica, pomak); 

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnikBaze, lozinkaBaze)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upit);
            preparedStmt.setInt(1, granica);
            preparedStmt.setInt(2, pomak);
            preparedStmt.execute();
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                Dnevnik dnevnik = new Dnevnik();
                dnevnik.setId(odgovor.getInt("id"));
                dnevnik.setKorisnik(odgovor.getString("korisnik"));
                dnevnik.setUrl(odgovor.getString("url"));
                dnevnik.setIpAdresa(odgovor.getString("ipadresa"));
                dnevnik.setStatus(odgovor.getInt("status"));
                dnevnik.setTrajanje(odgovor.getInt("trajanje"));
                zapisi.add(dnevnik);
            }
            veza.close();

        } catch (SQLException ex) {
            Logger.getLogger(Dnevnik.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String pocetno() {
        return "pocetno";
    }

    public String pregledKorisnika() {
        return "pregledKorisnika";
    }

    public String prethodniZapisi() {
        if (korak - brojZapisaKojiSeMoguPrikazati > 0) {
            korak -= brojZapisaKojiSeMoguPrikazati;
        } else {
            korak = 0;
            prethodni = false;
        }
        sljedeci = true;
        this.dohvatiZapise();
        return "prethodniZapisi";
    }

    public String sljedeciZapisi() {
        prethodni = true;
        korak += brojZapisaKojiSeMoguPrikazati;
        this.dohvatiZapise();
        return "sljedeciZapisi";
    }

    public ServletContext getSc() {
        return sc;
    }

    public void setSc(ServletContext sc) {
        this.sc = sc;
    }

    public Konfiguracija getKonf() {
        return konf;
    }

    public void setKonf(Konfiguracija konf) {
        this.konf = konf;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getKorisnikBaze() {
        return korisnikBaze;
    }

    public void setKorisnikBaze(String korisnikBaze) {
        this.korisnikBaze = korisnikBaze;
    }

    public String getLozinkaBaze() {
        return lozinkaBaze;
    }

    public void setLozinkaBaze(String lozinkaBaze) {
        this.lozinkaBaze = lozinkaBaze;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public List<Dnevnik> getZapisi() {
        return zapisi;
    }

    public void setZapisi(List<Dnevnik> zapisi) {
        this.zapisi = zapisi;
    }

    public int getBrojZapisaKojiSeMoguPrikazati() {
        return brojZapisaKojiSeMoguPrikazati;
    }

    public void setBrojZapisaKojiSeMoguPrikazati(int brojZapisaKojiSeMoguPrikazati) {
        this.brojZapisaKojiSeMoguPrikazati = brojZapisaKojiSeMoguPrikazati;
    }

    public int getBrojPocetnogZapisa() {
        return brojPocetnogZapisa;
    }

    public void setBrojPocetnogZapisa(int brojPocetnogZapisa) {
        this.brojPocetnogZapisa = brojPocetnogZapisa;
    }

    public int getBrojZadnjegZapisa() {
        return brojZadnjegZapisa;
    }

    public void setBrojZadnjegZapisa(int brojZadnjegZapisa) {
        this.brojZadnjegZapisa = brojZadnjegZapisa;
    }

    public int getPrvi() {
        return prvi;
    }

    public void setPrvi(int prvi) {
        this.prvi = prvi;
    }

    public int getKorak() {
        return korak;
    }

    public void setKorak(int korak) {
        this.korak = korak;
    }

    public boolean isNemaZapisa() {
        return nemaZapisa;
    }

    public void setNemaZapisa(boolean nemaZapisa) {
        this.nemaZapisa = nemaZapisa;
    }

    public boolean isSljedeci() {
        return sljedeci;
    }

    public void setSljedeci(boolean sljedeci) {
        this.sljedeci = sljedeci;
    }

    public boolean isPrethodni() {
        return prethodni;
    }

    public void setPrethodni(boolean prethodni) {
        this.prethodni = prethodni;
    }

    public int getIdZapisa() {
        return idZapisa;
    }

    public void setIdZapisa(int idZapisa) {
        this.idZapisa = idZapisa;
    }

    public Date getOdVremena() {
        return odVremena;
    }

    public void setOdVremena(Date odVremena) {
        this.odVremena = odVremena;
    }

    public Date getDoVremena() {
        return doVremena;
    }

    public void setDoVremena(Date doVremena) {
        this.doVremena = doVremena;
    }

    public String getAdresaZahtjeva() {
        return adresaZahtjeva;
    }

    public void setAdresaZahtjeva(String adresaZahtjeva) {
        this.adresaZahtjeva = adresaZahtjeva;
    }

    public String getKorisnikZahtjeva() {
        return korisnikZahtjeva;
    }

    public void setKorisnikZahtjeva(String korisnikZahtjeva) {
        this.korisnikZahtjeva = korisnikZahtjeva;
    }

//    private String sastaviUpit(int granica, int pomak) {
//        String upit = "SELECT * FROM dnevnik LIMIT ? OFFSET ?";
//    }
}
