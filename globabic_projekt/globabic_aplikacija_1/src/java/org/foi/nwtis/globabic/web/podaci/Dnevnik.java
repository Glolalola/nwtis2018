/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.podaci;

import java.util.Date;

/**
 *
 * @author Gloria Babić
 */
public class Dnevnik {

    private int id;
    private String korisnik;
    private String url;
    private String ipAdresa = "";
    private int trajanje;
    private int status;

    public Dnevnik() {
    }

    public Dnevnik(String korisnik, String url, int trajanje, int status) {
        this.korisnik = korisnik;
        this.url = url;
        this.trajanje = trajanje;
        this.status = status;
    }  

    public Dnevnik(int id, String korisnik, String url, int trajanje, int status) {
        this.id = id;
        this.korisnik = korisnik;
        this.url = url;
        this.trajanje = trajanje;
        this.status = status;
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(String korisnik) {
        this.korisnik = korisnik;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIpAdresa() {
        return ipAdresa;
    }

    public void setIpAdresa(String ipAdresa) {
        this.ipAdresa = ipAdresa;
    }

    public int getTrajanje() {
        return trajanje;
    }

    public void setTrajanje(int trajanje) {
        this.trajanje = trajanje;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

   

}
