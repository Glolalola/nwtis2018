package org.foi.nwtis.globabic.web.email;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Gloria Babić
 */
public class Email {

    private String salje;
    private String prima;
    private String predmet;
    private String sadrzaj;

    /**
     *
     * @param salje
     * @param prima
     * @param predmet
     * @param sadrzaj
     */
    public Email(String salje, String prima, String predmet, String sadrzaj) {
        this.salje = salje;
        this.prima = prima;
        this.predmet = predmet;
        this.sadrzaj = sadrzaj;
    }

    /**
     *
     * @return
     * @throws NamingException
     * @throws MessagingException
     */
    public String posalji() throws NamingException, MessagingException {

        Context context = new InitialContext();
        Context envContext = (Context) context.lookup("java:comp/env");
        Session session = (Session) envContext.lookup("mail/Session");

        Message message = new MimeMessage(session);
        Address fromAddress = new InternetAddress(salje);
        message.setFrom(fromAddress);

        Address[] toAddresses = InternetAddress.parse(prima);

        message.setRecipients(Message.RecipientType.TO, toAddresses);
        message.setSubject(predmet);
        message.setText(sadrzaj);
        Transport.send(message);

        return "PoslanEmail";
    }

    /**
     *
     * @return
     */
    public String getSalje() {
        return salje;
    }

    /**
     *
     * @param salje
     */
    public void setSalje(String salje) {
        this.salje = salje;
    }

    /**
     *
     * @return
     */
    public String getPrima() {
        return prima;
    }

    /**
     *
     * @param prima
     */
    public void setPrima(String prima) {
        this.prima = prima;
    }

    /**
     *
     * @return
     */
    public String getPredmet() {
        return predmet;
    }

    /**
     *
     * @param predmet
     */
    public void setPredmet(String predmet) {
        this.predmet = predmet;
    }

    /**
     *
     * @return
     */
    public String getSadrzaj() {
        return sadrzaj;
    }

    /**
     *
     * @param sadrzaj
     */
    public void setSadrzaj(String sadrzaj) {
        this.sadrzaj = sadrzaj;
    }
    
    

}
