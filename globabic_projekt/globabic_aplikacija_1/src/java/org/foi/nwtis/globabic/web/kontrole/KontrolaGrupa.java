/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.kontrole;

import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.web.slusaci.SlusacAplikacije;
import org.foi.nwtis.globabic.ws.klijenti.ParkiralistaWSKlijent;

/**
 *
 * @author Gloria Babić
 */
public class KontrolaGrupa {

    static Konfiguracija konfiguracija;
    static String svnKorime;
    static String svnLozinka;

    static String pozoviAkcijuGrupe(String akcija) {

        String odgovor = "";
        switch (akcija) {
            case "DODAJ":
                odgovor = dodaj();
                break;
            case "PREKID":
                odgovor = prekid();
                break;
            case "KRENI":
                odgovor = kreni();
                break;
            case "PAUZA":
                odgovor = pauza();
                break;
            case "STANJE":
                odgovor = stanje();
                break;
            default:
                break;
        }
        return odgovor;
    }

    private static String dodaj() {
        inicijaliziraj();
        String statusGrupe = ParkiralistaWSKlijent.dajStatusGrupe(svnKorime, svnLozinka).value();
        System.err.println("STATUS GRUPE; KOMANDA DODAJ: " + statusGrupe);
        String odgovor;
        if (statusGrupe.equals("REGISTRIRAN")) {
            return "ERR 20; Grupa je vec registrirana.";
        } else if (statusGrupe.equals("DEREGISTRIRAN")) {
            boolean rezultat = ParkiralistaWSKlijent.registrirajGrupu(svnKorime, svnLozinka);
            if (rezultat) {
                odgovor = "OK 20; Grupa je uspjesno registrirana.";
            } else {
                odgovor = "Dogodila se greska pri registraciji grupe.";
            }
        } else {
            odgovor = "Nije moguce registrirati grupu u stanju: " + statusGrupe;
        }

        return odgovor;
    }

    private static String prekid() {
        inicijaliziraj();
        String statusGrupe = ParkiralistaWSKlijent.dajStatusGrupe(svnKorime, svnLozinka).value();
        System.err.println("STATUS GRUPE; KOMANDA PREKID: " + statusGrupe);
        if (statusGrupe.equals("DEREGISTRIRAN")) {
            return "ERR 21; Grupa je vec deregistrirana.";
        }
        boolean rezultat = ParkiralistaWSKlijent.deregistrirajGrupu(svnKorime, svnLozinka);
        String odgovor;
        if (rezultat) {
            odgovor = "OK 20; Grupa je uspjesno deregistrirana.";
        } else {
            odgovor = "Dogodila se greska pri deregistraciji grupe.";
        }

        return odgovor;
    }

    private static String kreni() {
        inicijaliziraj();
        String statusGrupe = ParkiralistaWSKlijent.dajStatusGrupe(svnKorime, svnLozinka).value();
        System.err.println("STATUS GRUPE; KOMANDA KRENI: " + statusGrupe);
        if (statusGrupe.equals("DEREGISTRIRAN")) {
            return "ERR 21; Grupa ne postoji.";
        } else if (statusGrupe.equals("AKTIVAN")) {
            return "ERR 22; Grupa je već aktivirana.";
        }
        boolean rezultat = ParkiralistaWSKlijent.aktivirajGrupu(svnKorime, svnLozinka);
        String odgovor;
        if (rezultat) {
            odgovor = "OK 20; Grupa je uspjesno aktivirana.";
        } else {
            odgovor = "Dogodila se greska pri aktivaciji grupe.";
        }

        return odgovor;
    }

    private static String pauza() {
        inicijaliziraj();
        String statusGrupe = ParkiralistaWSKlijent.dajStatusGrupe(svnKorime, svnLozinka).value();
        System.err.println("STATUS GRUPE; KOMANDA KRENI: " + statusGrupe);
        if (statusGrupe.equals("DEREGISTRIRAN")) {
            return "ERR 21; Grupa ne postoji.";
        } else if (statusGrupe.equals("BLOKIRAN")) {
            return "ERR 22; Grupa je vec blokirana.";
        }
        boolean rezultat = ParkiralistaWSKlijent.blokirajGrupu(svnKorime, svnLozinka);
        String odgovor;
        if (rezultat) {
            odgovor = "OK 20; Grupa je uspješno blokirana.";
        } else {
            odgovor = "Dogodila se greska pri blokiranju grupe.";
        }

        return odgovor;
    }

    private static String stanje() {
        inicijaliziraj();
        org.foi.nwtis.globabic.ws.klijenti.StatusKorisnika status = ParkiralistaWSKlijent.dajStatusGrupe(svnKorime, svnLozinka);

        String odgovor = "";
        if (null != status) {
            switch (status) {
                case AKTIVAN:
                    odgovor = "OK 21; Grupa je aktivna.";
                    break;
                case BLOKIRAN:
                    odgovor = "OK 22; Grupa je blokirana.";
                    break;
                case REGISTRIRAN:
                    odgovor = "OK 23; Grupa je registrirana.";
                    break;
                case DEREGISTRIRAN:
                    odgovor = "ERR 21; Grupa je deregistrirana.";
                    break;
                case NEAKTIVAN:
                    odgovor = "ERR 21; Grupa je neaktivna.";
                    break;
                case NEPOSTOJI:
                    odgovor = "ERR 21; Grupa ne postoji.";
                    break;
                case PASIVAN:
                    odgovor = "ERR 21; Grupa je pasivna.";
                    break;
                default:
                    odgovor = "ERR 21; Grupa nema stanje.";
                    break;
            }
        }

        return odgovor;
    }

    private static void inicijaliziraj() {
        konfiguracija = SlusacAplikacije.konfiguracija;
        svnKorime = konfiguracija.dajPostavku("subversionUsername");
        svnLozinka = konfiguracija.dajPostavku("subversionPassword");
    }

}
