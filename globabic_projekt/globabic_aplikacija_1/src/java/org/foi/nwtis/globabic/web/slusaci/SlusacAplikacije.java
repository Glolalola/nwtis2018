package org.foi.nwtis.globabic.web.slusaci;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.konfiguracije.KonfiguracijaApstraktna;
import org.foi.nwtis.globabic.konfiguracije.NeispravnaKonfiguracija;
import org.foi.nwtis.globabic.konfiguracije.NemaKonfiguracije;
import org.foi.nwtis.globabic.konfiguracije.bp.BP_Konfiguracija;
import org.foi.nwtis.globabic.web.dretve.DretvaServera;
import org.foi.nwtis.globabic.web.dretve.PozadinskaDretva;

/**
 * Inicijalizira kontekst i pokreće upravljačku dretvu
 *
 * @author Gloria Babić
 */
public class SlusacAplikacije implements ServletContextListener {

    private static ServletContext servletContext;
    public static Konfiguracija konfiguracija = null;
    public static BP_Konfiguracija bP_Konfiguracija = null;
    private DretvaServera dretvaServera;
    private PozadinskaDretva pozadinskaDretva;

    public static ServletContext getServletContext() {
        return servletContext;
    }
    
    

    /**
     * Inicijalizira kontekst i pokreće pozadinsku dretvu
     *
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        servletContext = sce.getServletContext();
        String datoteka = servletContext.getInitParameter("konfiguracija");
        String putanja = servletContext.getRealPath("/WEB-INF") + java.io.File.separator;
        String puniNazivDatoteke = putanja + datoteka;

        bP_Konfiguracija = new BP_Konfiguracija(puniNazivDatoteke);
        servletContext.setAttribute("BP_Konfig", bP_Konfiguracija);

        try {
            konfiguracija = KonfiguracijaApstraktna.preuzmiKonfiguraciju(puniNazivDatoteke);
            servletContext.setAttribute("konfiguracija", konfiguracija);
            pozadinskaDretva = new PozadinskaDretva(konfiguracija);
            pozadinskaDretva.start();

            dretvaServera = new DretvaServera(konfiguracija);
            dretvaServera.start();

        } catch (NemaKonfiguracije | NeispravnaKonfiguracija ex) {
            Logger.getLogger(SlusacAplikacije.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     *
     * @param sce
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (dretvaServera != null) {
            dretvaServera.interrupt();
        }

        if (pozadinskaDretva != null) {
            pozadinskaDretva.interrupt();
        }
    }
}
