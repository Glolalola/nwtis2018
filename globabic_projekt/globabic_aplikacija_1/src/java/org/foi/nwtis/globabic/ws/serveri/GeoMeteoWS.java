package org.foi.nwtis.globabic.ws.serveri;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.konfiguracije.bp.BP_Konfiguracija;
import org.foi.nwtis.globabic.rest.klijenti.OWMKlijent;
import org.foi.nwtis.globabic.web.podaci.Lokacija;
import org.foi.nwtis.globabic.web.podaci.MeteoPodaci;
import org.foi.nwtis.globabic.web.podaci.Parkiraliste;
import org.foi.nwtis.globabic.web.slusaci.SlusacAplikacije;

/**
 * SOAP web servis za meteorološke podatke spremljenih parkirališta. Operacije
 * se temelje na podacima koje se nalaze u tablici METEO u bazi podataka. Sadrži
 * sljedeće operacije: daje popis svih parkirališta i njihovih geo lokacija,
 * vraća java.util.List<Parkiraliste> ili u application/json formatu
 *
 * dodaj parkiralište (šalje se naziv i adresa koja služi za preuzimanje geo
 * lokacije)
 *
 * svi meteo podaci za parkiralište u intervalu (šalju se id parkirališta, od i
 * do tipa long za timestamp, vraća se niz objekata klase Meteo) (id je
 * identifikator parkirališta u tablici PARKIRALISTAu bazi podataka)
 *
 * zadnji meteo podaci za parkiralište (šalje se id parkirališta, vraća se
 * objekat klase Meteo) (id je identifikator parkirališta u tablici
 * PARKIRALISTAu bazi podataka)
 *
 * važeći meteo podaci za parkiralište (šalje se id parkirališta, vraća se
 * objekat klase Meteo) (id je identifikator parkirališta u tablici
 * PARKIRALISTAu bazi podataka). Podaci se preuzimaju od Web servisa
 * openweathermap.org
 *
 * min i max važeća temperatura za parliralište u intervalu (šalju se id
 * parkirališta, od i do tipa long za timestamp, vraća se niz od dva elementa
 * tipa float) (id je identifikator parkirališta u tablici PARKIRALISTA u bazi
 * podataka)
 *
 *
 * @author Gloria Babić
 */
@WebService(serviceName = "GeoMeteoWS")
public class GeoMeteoWS {

    private ServletContext sc = null;
    private Konfiguracija konf;
    private String url;
    private String korisnik;
    private String lozinka;
    private String driver;
    private String apikey;
    private long startTime;

    @Resource
    private WebServiceContext context;

    /**
     * Web service operation
     *
     * svi meteo podaci za parkiralište u intervalu (šalju se id parkirališta,
     * od i do tipa long za timestamp, vraća se niz objekata klase Meteo) (id je
     * identifikator parkirališta u tablici PARKIRALISTA u bazi podataka)
     *
     * @param korisnickoIme
     * @param lozinkaKorisnika
     * @param idParkiralista
     * @param odVremena
     * @param doVremena
     * @return
     */

    @WebMethod(operationName = "dajSveMeteoPodatke")
    public java.util.List<MeteoPodaci> dajSveMeteoPodatke(@WebParam(name = "korime") String korisnickoIme, @WebParam(name = "lozinka") String lozinkaKorisnika, @WebParam(name = "idParkiralista") int idParkiralista, @WebParam(name = "odVremena") long odVremena, @WebParam(name = "doVremena") long doVremena) {
        startTime = System.currentTimeMillis();
        if (autenticirajKorisnika(korisnickoIme, lozinkaKorisnika)) {
            upisiUDnevnik("dajSveMeteoPodatke", korisnickoIme, 1);
            return dohvatiMeteoPodatke(idParkiralista, odVremena, doVremena);
        } else {
            upisiUDnevnik("dajSveMeteoPodatke", korisnickoIme, 0);
            return null;
        }
    }

    /**
     * Web service operation
     *
     * zadnji meteo podaci za parkiralište (šalje se id parkirališta, vraća se
     * objekat klase Meteo) (id je identifikator parkirališta u tablici
     * PARKIRALISTA u bazi podataka)
     *
     * @param korisnickoIme
     * @param lozinkaKorisnika
     * @param id
     * @return
     */
    @WebMethod(operationName = "dajZadnjeMeteoPodatke")
    public MeteoPodaci dajZadnjeMeteoPodatke(@WebParam(name = "korime") String korisnickoIme, @WebParam(name = "lozinka") String lozinkaKorisnika, @WebParam(name = "id") int id) {
        startTime = System.currentTimeMillis();
        if (autenticirajKorisnika(korisnickoIme, lozinkaKorisnika)) {
            upisiUDnevnik("dajZadnjeMeteoPodatke", korisnickoIme, 1);
            return dohvatiZadnjeMeteoPodatke(id);
        } else {
            upisiUDnevnik("dajZadnjeMeteoPodatke", korisnickoIme, 0);
            return null;
        }

    }

    /**
     * Web service operation
     *
     * važeći meteo podaci za parkiralište (šalje se id parkirališta, vraća se
     * objekat klase Meteo) (id je identifikator parkirališta u tablici
     * PARKIRALISTA u bazi podataka). Podaci se preuzimaju od Web servisa
     * openweathermap.org
     *
     * @param korisnickoIme
     * @param lozinkaKorisnika
     * @param id
     * @return
     */
    @WebMethod(operationName = "dajVazeceMeteoPodatke")
    public MeteoPodaci dajVazeceMeteoPodatke(@WebParam(name = "korime") String korisnickoIme, @WebParam(name = "lozinka") String lozinkaKorisnika, @WebParam(name = "id") int id) {
        startTime = System.currentTimeMillis();
        if (autenticirajKorisnika(korisnickoIme, lozinkaKorisnika)) {
            upisiUDnevnik("dajVazeceMeteoPodatke", korisnickoIme, 1);
            return dohvatiVazeceMeteoPodatke(id);
        } else {
            upisiUDnevnik("dajVazeceMeteoPodatke", korisnickoIme, 0);
            return null;
        }
    }

    /**
     * Web service operation
     *
     * zadnjih n meteo podataka za parkiralište (šalje se id parkirališta i n,
     * vraća se objekat klase Meteo) (id je identifikator parkirališta u tablici
     * PARKIRALISTA u bazi podataka). Podaci se preuzimaju od Web servisa
     * openweathermap.org
     *
     * @param korisnickoIme
     * @param lozinkaKorisnika
     * @param id
     * @param n
     * @return
     */
    @WebMethod(operationName = "dajNMeteoPodatka")
    public ArrayList<MeteoPodaci> dajNMeteoPodatka(@WebParam(name = "korime") String korisnickoIme, @WebParam(name = "lozinka") String lozinkaKorisnika, @WebParam(name = "id") int id, @WebParam(name = "n") int n) {
        startTime = System.currentTimeMillis();
        if (autenticirajKorisnika(korisnickoIme, lozinkaKorisnika)) {
            upisiUDnevnik("dajNMeteoPodatka", korisnickoIme, 1);
            return dohvatiNMeteoPodatka(id, n);
        } else {
            upisiUDnevnik("dajNMeteoPodatka", korisnickoIme, 0);
            return null;
        }
    }

    /**
     * Inicijalizira podatke potrebne za rad sa bazom. Dohvaća podatke iz
     * konfiguracija.
     */
    private void inicijaliziraj() {
        sc = SlusacAplikacije.getServletContext();
        konf = (Konfiguracija) sc.getAttribute("konfiguracija");

        BP_Konfiguracija bpk = (BP_Konfiguracija) sc.getAttribute("BP_Konfig");
        if (bpk == null) {
            System.err.println("Problem s konfiguracijom!");
            return;
        }
        url = bpk.getServerDatabase() + bpk.getAdminDatabase();
        korisnik = bpk.getAdminUsername();
        lozinka = bpk.getAdminPassword();
        driver = bpk.getDriverDatabase();
        apikey = konf.dajPostavku("apikey");
    }

    /**
     * dohvaća sve meteo podatke za parkiralište u intervalu
     *
     * @param idParkiralista
     * @param odVremena
     * @param doVremena
     * @return
     */
    private ArrayList<MeteoPodaci> dohvatiMeteoPodatke(int idParkiralista, long odVremena, long doVremena) {
        inicijaliziraj();
        ArrayList<MeteoPodaci> mp = new ArrayList<>();
        String upitMeteo = "SELECT * FROM METEO WHERE ID =? AND PREUZETO >= ? and PREUZETO <= ?";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitMeteo);
            preparedStmt.setInt(1, idParkiralista);
            preparedStmt.setTimestamp(2, new Timestamp(odVremena));
            preparedStmt.setTimestamp(3, new Timestamp(doVremena));
            preparedStmt.execute();
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                mp.add(popuniMeteo(odgovor));
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(GeoMeteoWS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mp;
    }

    /**
     * popunjava meteo podatke s rezultatima iz baze
     *
     * @param odgovor
     * @param mp
     * @throws NumberFormatException
     * @throws SQLException
     */
    private MeteoPodaci popuniMeteo(ResultSet odgovor) throws NumberFormatException, SQLException {
        MeteoPodaci meteo = new MeteoPodaci();
        meteo.setWeatherNumber(Integer.valueOf(odgovor.getString("VRIJEME")));
        meteo.setWeatherValue(odgovor.getString("VRIJEMEOPIS"));
        meteo.setTemperatureValue(odgovor.getFloat("TEMP"));
        meteo.setTemperatureMin(odgovor.getFloat("TEMPMIN"));
        meteo.setTemperatureMax(odgovor.getFloat("TEMPMAX"));
        meteo.setHumidityValue(odgovor.getFloat("VLAGA"));
        meteo.setPressureValue(odgovor.getFloat("TLAK"));
        meteo.setWindSpeedValue(odgovor.getFloat("VJETAR"));
        meteo.setWindDirectionValue(odgovor.getFloat("VJETARSMJER"));
        meteo.setLastUpdate(odgovor.getDate("preuzeto"));
        return meteo;
    }

    /**
     * dohvaća zadnje meteo podatke za parkiralište s danim id-jem
     *
     * @param id
     * @return
     */
    private MeteoPodaci dohvatiZadnjeMeteoPodatke(int id) {
        inicijaliziraj();
        MeteoPodaci meteo = null;
        String upitMeteo = "SELECT * FROM METEO WHERE ID =? ORDER BY PREUZETO";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitMeteo);
            preparedStmt.setInt(1, id);
            preparedStmt.execute();
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                meteo = popuniMeteo(odgovor);
            }
            veza.close();

        } catch (SQLException ex) {
            Logger.getLogger(GeoMeteoWS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return meteo;
    }

    /**
     * dohvaća važeće meteo podatke za parkiralište s danim id-jem
     *
     * @param id
     * @return
     */
    private MeteoPodaci dohvatiVazeceMeteoPodatke(int id) {
        inicijaliziraj();
        String upitParkiralista = "SELECT * FROM PARKIRALISTA WHERE ID = ?";
        Parkiraliste parkiraliste = new Parkiraliste();
        Lokacija lokacija = new Lokacija();
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitParkiralista);
            preparedStmt.setInt(1, id);
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                parkiraliste.setId(odgovor.getInt("ID"));
                parkiraliste.setNaziv(odgovor.getString("NAZIV"));
                parkiraliste.setAdresa("ADRESA");
                lokacija.setLatitude(odgovor.getString("LATITUDE"));
                lokacija.setLongitude(odgovor.getString("LONGITUDE"));
                parkiraliste.setGeoloc(lokacija);
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(GeoMeteoWS.class.getName()).log(Level.SEVERE, null, ex);
        }
        OWMKlijent owmk = new OWMKlijent(apikey);
        MeteoPodaci mp = owmk.getRealTimeWeather(lokacija.getLatitude(), lokacija.getLongitude());
        return mp;
    }

    /**
     * dohvaća zadnje meteo podatke za parkiralište s danim id-jem
     *
     * @param id
     * @return
     */
    private ArrayList<MeteoPodaci> dohvatiNMeteoPodatka(int id, int n) {
        inicijaliziraj();
        ArrayList<MeteoPodaci> mp = new ArrayList<>();
        String upitMeteo = "SELECT * FROM METEO WHERE ID =? ORDER BY PREUZETO";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitMeteo);
            preparedStmt.setInt(1, id);
            preparedStmt.execute();
            ResultSet odgovor = preparedStmt.executeQuery();
            int i = 0;
            while (odgovor.next() && i < n) {
                mp.add(popuniMeteo(odgovor));
                i++;
            }
            veza.close();

        } catch (SQLException ex) {
            Logger.getLogger(GeoMeteoWS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mp;
    }

    private boolean autenticirajKorisnika(String korisnickoIme, String lozinkaKorisnika) {
        inicijaliziraj();
        boolean postojiKorisnik = false;
        String upitKorisnik = "SELECT * FROM korisnici WHERE kor_ime=? AND lozinka=?";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitKorisnik);
            preparedStmt.setString(1, korisnickoIme);
            preparedStmt.setString(2, lozinkaKorisnika);
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                postojiKorisnik = true;
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(GeoMeteoWS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return postojiKorisnik;

    }

    private boolean upisiUDnevnik(String akcija, String korisnickoIme, int status) {
        inicijaliziraj();
        boolean dodano = false;
        HttpServletRequest hsr = (HttpServletRequest) context.getMessageContext()
                .get(MessageContext.SERVLET_REQUEST);
        String upitParkiralista = "INSERT INTO dnevnik (KORISNIK, URL, IPADRESA, VRIJEME, TRAJANJE, STATUS) values(?,?,?,?,?,?)";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitParkiralista);
            preparedStmt.setString(1, korisnickoIme);
            preparedStmt.setString(2, hsr.getRequestURL().toString() + "/" + akcija);
            preparedStmt.setString(3, hsr.getRemoteAddr());
            preparedStmt.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            preparedStmt.setLong(5, System.currentTimeMillis() - startTime);
            preparedStmt.setInt(6, status);
            preparedStmt.execute();
            veza.close();
            dodano = true;
        } catch (SQLException ex) {
            Logger.getLogger(GeoMeteoWS.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dodano;
    }

}
