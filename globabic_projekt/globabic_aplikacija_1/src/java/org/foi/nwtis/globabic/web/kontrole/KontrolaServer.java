/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.kontrole;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import org.foi.nwtis.globabic.web.dretve.DretvaServera;
import org.foi.nwtis.globabic.web.podaci.Korisnik;

/**
 *
 * @author Gloria Babić
 */
public class KontrolaServer {

    public static String pozoviAkcijuServera(String akcija) {
        String odgovor = "";
        switch (akcija) {
            case "PAUZA":
                odgovor = pauza();
                break;
            case "KRENI":
                odgovor = kreni();
                break;
            case "PASIVNO":
                odgovor = pasivno();
                break;
            case "AKTIVNO":
                odgovor = aktivno();
                break;
            case "STANI":
                odgovor = stani();
                break;
            case "STANJE":
                odgovor = stanje();
                break;
            case "LISTAJ":
                odgovor = listaj();
                break;
            default:
                break;
        }
        return odgovor;
    }

    private static String pauza() {
        if (DretvaServera.serverPrimaKomande == false) {
            return "ERR 12;";
        }

        DretvaServera.serverPrimaKomande = false;
        return "OK 10; ";
    }

    private static String kreni() {
        if (DretvaServera.serverPrimaKomande == true) {
            return "ERR 13;";
        }

        DretvaServera.serverPrimaKomande = true;
        return "OK 10; ";
    }

    private static String pasivno() {
        if (DretvaServera.serverPrimaMeteo == false) {
            return "ERR 14;";
        }

        DretvaServera.serverPrimaMeteo = false;
        return "OK 10; ";
    }

    private static String aktivno() {
        if (DretvaServera.serverPrimaMeteo == true) {
            return "ERR 14;";
        }

        DretvaServera.serverPrimaMeteo = true;
        return "OK 10; ";
    }

    private static String stani() {
        if (DretvaServera.serverSeZaustavlja == true) {
            return "ERR 16;";
        }

        DretvaServera.serverSeZaustavlja = true;
        return "OK 10; ";
    }

    private static String stanje() {
        String odgovor = "";
        if (DretvaServera.serverPrimaKomande && DretvaServera.serverPrimaMeteo) {
            odgovor = "OK 11;";
        } else if (DretvaServera.serverPrimaKomande && !DretvaServera.serverPrimaMeteo) {
            odgovor = "OK 12;";
        } else if (!DretvaServera.serverPrimaKomande && DretvaServera.serverPrimaMeteo) {
            odgovor = "OK 13;";
        } else if (!DretvaServera.serverPrimaKomande && !DretvaServera.serverPrimaMeteo) {
            odgovor = "OK 14;";
        }

        return odgovor;
    }

    private static String listaj() {
        List<Korisnik> korisnici = new ArrayList<>();
        korisnici = KontrolaKorisnika.dohvatiKorisnike();
        if (korisnici.isEmpty()) {
            return "ERR 17; ";
        } else {
            JsonArrayBuilder jsonBuilder = Json.createArrayBuilder();
            for (Korisnik korisnik : korisnici) {
                jsonBuilder.add(Json.createObjectBuilder()
                        .add("ki", korisnik.getKorime())
                        .add("prezime", korisnik.getPrezime())
                        .add("ime", korisnik.getIme())
                        .add("email", korisnik.getEmail())
                        .add("vrsta", korisnik.getVrsta()));
            }

            JsonArray korisniciJsonDio = jsonBuilder.build();
            return "OK 10; " + korisniciJsonDio.toString();
        }
    }

    public static String pozoviAkcijuKorisnik(String korime, String lozinka, String akcija, String ime, String prezime) {
        String odgovor = "";
        switch (akcija) {
            case "DODAJ":
                odgovor = dodaj(korime, lozinka, ime, prezime);
                break;
            case "AZURIRAJ":
                odgovor = azuriraj(korime, ime, prezime);
                break;
            default:
                break;
        }
        return odgovor;
    }

    private static String dodaj(String korime, String lozinka, String ime, String prezime) {
        String odgovor = "";
        if (KontrolaKorisnika.postojiLiKorisnik(korime)) {
            odgovor = "ERR 10;";
        } else {
            KontrolaKorisnika.dodajNovogKorisnika(korime, lozinka, ime, prezime);
            odgovor = "OK 10; ";
        }

        return odgovor;
    }

    private static String azuriraj(String korime, String ime, String prezime) {
        String odgovor = "";

        if (KontrolaKorisnika.postojiLiKorisnik(korime) == false) {
            odgovor = "ERR 10;";
        } else {
            KontrolaKorisnika.azurirajKorisnika(korime, ime, prezime);
            odgovor = "OK 10; ";
        }

        return odgovor;
    }

    static String pozoviAkcijuKorisnikPreuzmi(String korime) {
        Korisnik korisnik = new Korisnik();
        korisnik = KontrolaKorisnika.vratiKorisnika(korime);

        if (korisnik.getKorime().isEmpty()) { 
            return "ERR 17;";
        } else {
            JsonObject jsonOdgovor;
            jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                    .add("ki", korisnik.getKorime())
                    .add("prezime", korisnik.getPrezime())
                    .add("ime", korisnik.getIme())
                    .add("email", korisnik.getEmail())
                    .add("vrsta", korisnik.getVrsta())
                    .build());
            String odgovor = jsonOdgovor.toString();

            return "OK 10; " + odgovor;
        }
    }

}
