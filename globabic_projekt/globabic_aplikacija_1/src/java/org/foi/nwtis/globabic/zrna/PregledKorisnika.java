/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.zrna;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.ServletContext;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.konfiguracije.bp.BP_Konfiguracija;
import org.foi.nwtis.globabic.web.podaci.Korisnik;
import org.foi.nwtis.globabic.web.slusaci.SlusacAplikacije;

/**
 *
 * @author Gloria Babić
 */
@ManagedBean
@SessionScoped
public class PregledKorisnika {

    private ServletContext sc = null;
    private Konfiguracija konf;
    private String url;
    private String korisnikBaze;
    private String lozinkaBaze;
    private String driver;

    private List<Korisnik> korisnici = new ArrayList<>();

    private int brojKorisnikaKojiSeMoguPrikazati;
    private int brojPocetnogKorisnika;
    private int brojZadnjegKorisnika;
    private int prvi = 0;
    private int korak;

    private boolean nemaKorisnika = true;
    private boolean sljedeci = true, prethodni = true;

    /**
     * Creates a new instance of PregledKorisnika
     */
    public PregledKorisnika() {
    }

    @PostConstruct
    public void init() {
        inicijaliziraj();
        dohvatiKorisnike();
    }

    private void inicijaliziraj() {
        System.out.println("inicijaliziraj");
        sc = SlusacAplikacije.getServletContext();
        konf = (Konfiguracija) sc.getAttribute("konfiguracija");

        BP_Konfiguracija bpk = (BP_Konfiguracija) sc.getAttribute("BP_Konfig");
        if (bpk == null) {
            System.err.println("Problem s konfiguracijom!");
            return;
        }
        url = bpk.getServerDatabase() + bpk.getAdminDatabase();
        korisnikBaze = bpk.getAdminUsername();
        lozinkaBaze = bpk.getAdminPassword();
        driver = bpk.getDriverDatabase();
        brojKorisnikaKojiSeMoguPrikazati = Integer.parseInt(konf.dajPostavku("brojPodatakaNastranici"));

        prvi = 0;
        prethodni = false;
    }

    private void dohvatiKorisnike() {
        korisnici.clear();
        if (prvi == 0) {
            dohvatiKorisnikeizBaze(brojKorisnikaKojiSeMoguPrikazati, 0);
            prvi++;
        } else {
            dohvatiKorisnikeizBaze(brojKorisnikaKojiSeMoguPrikazati, korak);
            if (korisnici.isEmpty()) {
                sljedeci = false;
            }
        }
    }

    private void dohvatiKorisnikeizBaze(int granica, int pomak) {
        String upit = "SELECT * FROM korisnici LIMIT ? OFFSET ?";

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnikBaze, lozinkaBaze)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upit);
            preparedStmt.setInt(1, granica);
            preparedStmt.setInt(2, pomak);
            preparedStmt.execute();
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                Korisnik korisnik = new Korisnik();
                korisnik.setId(odgovor.getInt("id"));
                korisnik.setKorime(odgovor.getString("kor_ime"));
                korisnik.setLozinka(odgovor.getString("lozinka"));
                korisnici.add(korisnik);
            }
            veza.close();

        } catch (SQLException ex) {
            Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String pocetno() {
        return "pocetno";
    }

    public String pregledDnevnika() {
        return "pregledDnevnika";
    }

    public String prethodniKorisnici() {
        if (korak - brojKorisnikaKojiSeMoguPrikazati > 0) {
            korak -= brojKorisnikaKojiSeMoguPrikazati;
        } else {
            korak = 0;
            prethodni = false;
        }
        sljedeci = true;
        this.dohvatiKorisnike();
        return "prethodniKorisnici";
    }

    public String sljedeciKorisnici() {
        prethodni = true;
        korak += brojKorisnikaKojiSeMoguPrikazati;
        this.dohvatiKorisnike();
        return "sljedeciKorisnici";
    }

    public ServletContext getSc() {
        return sc;
    }

    public void setSc(ServletContext sc) {
        this.sc = sc;
    }

    public Konfiguracija getKonf() {
        return konf;
    }

    public void setKonf(Konfiguracija konf) {
        this.konf = konf;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getKorisnikBaze() {
        return korisnikBaze;
    }

    public void setKorisnikBaze(String korisnikBaze) {
        this.korisnikBaze = korisnikBaze;
    }

    public String getLozinkaBaze() {
        return lozinkaBaze;
    }

    public void setLozinkaBaze(String lozinkaBaze) {
        this.lozinkaBaze = lozinkaBaze;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public List<Korisnik> getKorisnici() {
        return korisnici;
    }

    public void setKorisnici(List<Korisnik> korisnici) {
        this.korisnici = korisnici;
    }

    public int getBrojKorisnikaKojiSeMoguPrikazati() {
        return brojKorisnikaKojiSeMoguPrikazati;
    }

    public void setBrojKorisnikaKojiSeMoguPrikazati(int brojKorisnikaKojiSeMoguPrikazati) {
        this.brojKorisnikaKojiSeMoguPrikazati = brojKorisnikaKojiSeMoguPrikazati;
    }

    public int getBrojPocetnogKorisnika() {
        return brojPocetnogKorisnika;
    }

    public void setBrojPocetnogKorisnika(int brojPocetnogKorisnika) {
        this.brojPocetnogKorisnika = brojPocetnogKorisnika;
    }

    public int getBrojZadnjegKorisnika() {
        return brojZadnjegKorisnika;
    }

    public void setBrojZadnjegKorisnika(int brojZadnjegKorisnika) {
        this.brojZadnjegKorisnika = brojZadnjegKorisnika;
    }

    public int getPrvi() {
        return prvi;
    }

    public void setPrvi(int prvi) {
        this.prvi = prvi;
    }

    public int getKorak() {
        return korak;
    }

    public void setKorak(int korak) {
        this.korak = korak;
    }

    public boolean isNemaKorisnika() {
        return nemaKorisnika;
    }

    public void setNemaKorisnika(boolean nemaKorisnika) {
        this.nemaKorisnika = nemaKorisnika;
    }

    public boolean isSljedeci() {
        return sljedeci;
    }

    public void setSljedeci(boolean sljedeci) {
        this.sljedeci = sljedeci;
    }

    public boolean isPrethodni() {
        return prethodni;
    }

    public void setPrethodni(boolean prethodni) {
        this.prethodni = prethodni;
    }

}
