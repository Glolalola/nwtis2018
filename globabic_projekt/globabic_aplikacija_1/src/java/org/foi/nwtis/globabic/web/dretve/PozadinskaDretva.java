/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.dretve;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.konfiguracije.bp.BP_Konfiguracija;
import org.foi.nwtis.globabic.rest.klijenti.OWMKlijent;
import org.foi.nwtis.globabic.web.podaci.Lokacija;
import org.foi.nwtis.globabic.web.podaci.MeteoPodaci;
import org.foi.nwtis.globabic.web.podaci.Parkiraliste;
import org.foi.nwtis.globabic.web.slusaci.SlusacAplikacije;

/**
 * Preuzimaju se u pravilnim intervalima meteorološki podaci putem REST web
 * servisa openweathermap.org za izabrani skup parkirališta i pohranjuju se u
 * tablicu u bazi podataka (METEO)
 *
 * @author Gloria Babić
 */
public class PozadinskaDretva extends Thread {

    private ServletContext sc;
    private Konfiguracija konf;
    private String url;
    private String korisnik;
    private String lozinka;
    private String driver;
    private String apikey;
    private int interval;

    private static boolean radi;

    /**
     *
     * @param konf
     */
    public PozadinskaDretva(Konfiguracija konf) {
        this.konf = konf;
    }

    @Override
    public void interrupt() {
        super.interrupt(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Inicijalizira podatke potrebne za rad sa bazom. Dohvaća podatke iz
     * konfiguracija.
     */
    private void inicijaliziraj() {
        sc = SlusacAplikacije.getServletContext();
        konf = (Konfiguracija) sc.getAttribute("konfiguracija");

        BP_Konfiguracija bpk = (BP_Konfiguracija) sc.getAttribute("BP_Konfig");
        if (bpk == null) {
            System.err.println("Problem s konfiguracijom!");
            return;
        }
        url = bpk.getServerDatabase() + bpk.getAdminDatabase();
        korisnik = bpk.getAdminUsername();
        lozinka = bpk.getAdminPassword();
        driver = bpk.getDriverDatabase();
        apikey = konf.dajPostavku("apikey");
        interval = Integer.parseInt(konf.dajPostavku("intervalDretve"));
        
    }

    @Override
    public void run() {
        inicijaliziraj();
        System.err.println("POKRENUTA POZADINSKA DRETVA");
        radi = true;
        while (radi) {
            if (DretvaServera.serverSeZaustavlja) {
                radi = false;
                continue;
            }
            try {
                if (DretvaServera.serverPrimaMeteo) {
                    ArrayList<Parkiraliste> parkiralistaZaUnos = dohvatiParkiralista();
                    for (Parkiraliste parking : parkiralistaZaUnos) {
                        OWMKlijent client = new OWMKlijent(apikey);
                        try {
                            MeteoPodaci meteo = client.getRealTimeWeather(parking.getGeoloc().getLatitude(), parking.getGeoloc().getLongitude());
                            unesiMeteo(meteo, parking);
                        } catch (Exception ex) {

                        }
                    }
                } else {
                    System.err.println("Server trenutno ne preuzima Meteo.");
                }
                System.err.println("INTERVAL: " + interval * 1000);
                sleep(interval * 1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(PozadinskaDretva.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public synchronized void start() {
        super.start(); //To change body of generated methods, choose Tools | Templates.

    }

    /**
     * dohvaća sva parkirališta iz baze
     *
     * @return
     */
    private ArrayList<Parkiraliste> dohvatiParkiralista() {
        ArrayList<Parkiraliste> parkiralista = new ArrayList<>();
        String query = "SELECT * FROM PARKIRALISTA";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(query);
            preparedStmt.execute();
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                Parkiraliste parkiraliste = new Parkiraliste();
                parkiraliste.setId(odgovor.getInt("ID"));
                parkiraliste.setNaziv(odgovor.getString("NAZIV"));
                parkiraliste.setAdresa(odgovor.getString("ADRESA"));
                Lokacija lokacija = new Lokacija();
                lokacija.setLatitude(odgovor.getString("LATITUDE"));
                lokacija.setLongitude(odgovor.getString("LONGITUDE"));
                parkiraliste.setGeoloc(lokacija);
                parkiralista.add(parkiraliste);
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(PozadinskaDretva.class.getName()).log(Level.SEVERE, null, ex);
        }
        return parkiralista;
    }

    /**
     * dohvaća meteo podatke za dano parkiralište
     *
     * @param meteoPodaci
     * @param parkiraliste
     */
    private void unesiMeteo(MeteoPodaci meteoPodaci, Parkiraliste parkiraliste) {
        String insertString = "INSERT INTO METEO (ID, LATITUDE, LONGITUDE, VRIJEME, VRIJEMEOPIS, TEMP, TEMPMIN, TEMPMAX, VLAGA, TLAK, VJETAR, VJETARSMJER) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(insertString);
            String vrijeme = String.valueOf(meteoPodaci.getWeatherNumber());
            String vrijemeOpis = meteoPodaci.getWeatherValue();
            preparedStmt.setInt(1, parkiraliste.getId());
            preparedStmt.setString(2, parkiraliste.getGeoloc().getLatitude());
            preparedStmt.setString(3, parkiraliste.getGeoloc().getLongitude());
            preparedStmt.setString(4, vrijeme.substring(0, Math.min(24, vrijeme.length())));
            preparedStmt.setString(5, vrijemeOpis.substring(0, Math.min(24, vrijemeOpis.length())));
            preparedStmt.setDouble(6, meteoPodaci.getTemperatureValue());
            preparedStmt.setDouble(7, meteoPodaci.getTemperatureMin());
            preparedStmt.setDouble(8, meteoPodaci.getTemperatureMax());
            preparedStmt.setDouble(9, meteoPodaci.getHumidityValue());
            preparedStmt.setDouble(10, meteoPodaci.getPressureValue());
            preparedStmt.setDouble(11, meteoPodaci.getWindSpeedValue());
            preparedStmt.setDouble(12, meteoPodaci.getWindDirectionValue());
            preparedStmt.execute();
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(PozadinskaDretva.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean isRadi() {
        return radi;
    }

    public synchronized static void setRadi(boolean radi) {
        PozadinskaDretva.radi = radi;
    }

}
