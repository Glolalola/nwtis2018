/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.dretve;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.web.kontrole.KontrolaDnevnika;
import org.foi.nwtis.globabic.web.kontrole.KontrolaKomande;
import org.foi.nwtis.globabic.web.kontrole.KontrolaKorisnika;
import org.foi.nwtis.globabic.web.podaci.Dnevnik;

/**
 *
 * @author Gloria Babić
 */
public class DretvaServera extends Thread {

    private int redniBrojZadnjeDretve = 0;
    private final Konfiguracija konfiguracija;
    private ServerSocket serverSocket;
    private Socket socket = null;
    private int port;
    private long startTime;

    private boolean radi = false;
    public static boolean serverPrimaMeteo;
    public static boolean serverPrimaKomande;
    public static boolean serverSeZaustavlja;

    private String korisnik;
    private String lozinka;

    public DretvaServera(Konfiguracija konfiguracija) {
        this.konfiguracija = konfiguracija;
    }

    @Override
    public void run() {
        System.err.println("POKRENUTA DRETVA SERVERA");
        radi = true;
        serverPrimaMeteo = true;
        serverPrimaKomande = true;
        serverSeZaustavlja = false;
        try {
            pokreniServer();
        } catch (IOException ex) {
            Logger.getLogger(DretvaServera.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void interrupt() {
        radi = false;
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(DretvaServera.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        super.interrupt();
    }

    @Override
    public synchronized void start() {
        super.start(); //To change body of generated methods, choose Tools | Templates.
    }

    private void pokreniServer() throws IOException {
        Dnevnik dnevnik = new Dnevnik();
        startTime = System.currentTimeMillis();
        port = Integer.parseInt(konfiguracija.dajPostavku("port"));
        serverSocket = new ServerSocket(port);
        System.err.println("Server port: " + port);
        while (radi) {
            socket = serverSocket.accept();
            System.err.println("Stigla komanda?");
            if (serverSeZaustavlja) {
                radi = false;
                continue;
            }
            String komanda = KontrolaKomande.procitajKomandu(socket);
            Matcher matcher = KontrolaKomande.provjeriKomandu(komanda, KontrolaKomande.regexServerKomande);
            String odgovor = "";

            boolean izvrsenaKomanda = false;
            boolean poslanOdgovor = false;
            if (matcher.matches() == false) {
                odgovor = "Server neispravna komanda: " + komanda;
                System.out.println(odgovor);
                izvrsenaKomanda = true;
            } else if (komanda.contains("DODAJ") || komanda.contains("AZURIRAJ")) {
                KorisnickaDretva kd = new KorisnickaDretva(socket, "globabic - dretva " + redniBrojZadnjeDretve + " ", konfiguracija, komanda);
                kd.start();
                izvrsenaKomanda = true;
                poslanOdgovor = true;
                korisnik = matcher.group(1);
                dnevnik.setStatus(1);
            } else {
                korisnik = matcher.group(1);
                lozinka = matcher.group(2);
                if (KontrolaKorisnika.autenticirajKorisnika(korisnik, lozinka) == false) {
                    odgovor = "ERR 11;" + komanda;
                    System.out.println(odgovor);
                    izvrsenaKomanda = true;
                } else {
                    if (matcher.group(3).isEmpty()) {
                        odgovor = "OK 10; " + komanda;
                        System.out.println(odgovor);
                        izvrsenaKomanda = true;
                        dnevnik.setStatus(1);
                    }
                }
            }

            if (izvrsenaKomanda == true) {
                if (!poslanOdgovor) {
                    KontrolaKomande.odgovoriNaKomandu(odgovor, socket);
                }
            } else {
                dnevnik.setStatus(1);
                redniBrojZadnjeDretve++;
                RadnaDretva radnaDretva = new RadnaDretva(socket, "globabic - dretva " + redniBrojZadnjeDretve + " ", konfiguracija, komanda);
                radnaDretva.start();
            }
            if (korisnik.isEmpty() || korisnik == null) {
                korisnik = "anonimno";
            }
            dnevnik.setKorisnik(korisnik);
            dnevnik.setUrl(komanda);
            dnevnik.setTrajanje((int) (System.currentTimeMillis() - startTime));
            dnevnik.setIpAdresa(InetAddress.getLocalHost().getHostAddress());
            KontrolaDnevnika.spremiDnevnik(dnevnik);
        }
    }

}
