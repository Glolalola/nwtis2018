
package org.foi.nwtis.globabic.rest.klijenti;

/**
 *
 * @author Gloria Babić
 */
public class GMRESTHelper {
    private static final String GM_BASE_URI = "https://maps.google.com/";    

    /**
     *
     */
    public GMRESTHelper() {
    }

    /**
     *
     * @return
     */
    public static String getGM_BASE_URI() {
        return GM_BASE_URI;
    }
        
}
