/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.dretve;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.web.kontrole.KontrolaKomande;

/**
 *
 * @author Gloria Babić
 */
public class KorisnickaDretva extends Thread {

    private Socket socket;
    private Konfiguracija konfiguracija;
    private String komanda;
    private String imeDretve;
    private long startTime;
    private String odgovor;

    KorisnickaDretva(Socket socket, String imeDretve, Konfiguracija konfiguracija, String komanda) {
        this.socket = socket;
        this.imeDretve = imeDretve;
        this.konfiguracija = konfiguracija;
        this.komanda = komanda;
    }

    @Override
    public void interrupt() {
        super.interrupt(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void run() {
        System.err.println("POKRENUTA KORISNICKA DRETVA");
        startTime = System.currentTimeMillis();
        System.out.println("KORISNICKA DRETVA: " + imeDretve + " komanda: " + komanda);

        odgovor = KontrolaKomande.izvrsiKomandu(komanda);
        System.out.println("KORISNICKA DRETVA: " + imeDretve + " odgovor: " + odgovor);
        posaljiOdgovor(odgovor);
    }

    @Override
    public synchronized void start() {
        super.start(); //To change body of generated methods, choose Tools | Templates.
    }

    private void posaljiOdgovor(String odgovor) {
        try {
            OutputStream os;
            os = socket.getOutputStream();
            os.write(odgovor.getBytes());
            os.flush();
            socket.shutdownOutput();
        } catch (IOException ex) {
            Logger.getLogger(RadnaDretva.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Konfiguracija getKonfiguracija() {
        return konfiguracija;
    }

    public void setKonfiguracija(Konfiguracija konfiguracija) {
        this.konfiguracija = konfiguracija;
    }

    public String getKomanda() {
        return komanda;
    }

    public void setKomanda(String komanda) {
        this.komanda = komanda;
    }

    public String getImeDretve() {
        return imeDretve;
    }

    public void setImeDretve(String imeDretve) {
        this.imeDretve = imeDretve;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

}
