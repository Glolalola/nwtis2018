/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.kontrole;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.foi.nwtis.globabic.konfiguracije.bp.BP_Konfiguracija;
import org.foi.nwtis.globabic.web.podaci.Korisnik;
import org.foi.nwtis.globabic.web.slusaci.SlusacAplikacije;

/**
 *
 * @author Gloria Babić
 */
public class KontrolaKorisnika {

    private static String url;
    private static String korisnikBaze;
    private static String lozinkaBaze;
    private static String driver;
    private static final ServletContext sc = SlusacAplikacije.getServletContext();

    public static boolean autenticirajKorisnika(String korisnickoIme, String lozinkaKorisnika) {
        inicijaliziraj();
        boolean postojiKorisnik = false;
        String upitKorisnik = "SELECT * FROM korisnici WHERE kor_ime=? AND lozinka=?";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnikBaze, lozinkaBaze)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitKorisnik);
            preparedStmt.setString(1, korisnickoIme);
            preparedStmt.setString(2, lozinkaKorisnika);
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                postojiKorisnik = true;
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(KontrolaKorisnika.class.getName()).log(Level.SEVERE, null, ex);
        }
        return postojiKorisnik;

    }

    private static void inicijaliziraj() {
        BP_Konfiguracija bpk = (BP_Konfiguracija) sc.getAttribute("BP_Konfig");
        if (bpk == null) {
            System.err.println("Problem s konfiguracijom!");
            return;
        }
        url = bpk.getServerDatabase() + bpk.getAdminDatabase();
        korisnikBaze = bpk.getAdminUsername();
        lozinkaBaze = bpk.getAdminPassword();
        driver = bpk.getDriverDatabase();

    }

    public static List<Korisnik> dohvatiKorisnike() {
        inicijaliziraj();
        List<Korisnik> korisnici = new ArrayList<>();
        String upitKorisnik = "SELECT * FROM korisnici";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnikBaze, lozinkaBaze)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitKorisnik);
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                Korisnik k = new Korisnik();
                k.setId(odgovor.getInt("ID"));
                k.setVrsta(odgovor.getString("VRSTA"));
                k.setIme(odgovor.getString("IME"));
                k.setPrezime(odgovor.getString("PREZIME"));
                k.setKorime(odgovor.getString("KOR_IME"));
                k.setEmail(odgovor.getString("EMAIL_ADRESA"));
                k.setDatumKreiranja(odgovor.getDate("DATUM_KREIRANJA"));
                k.setDatumPromjene(odgovor.getDate("DATUM_PROMJENE"));
                k.setLozinka("skriveno");
                korisnici.add(k);
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(KontrolaKorisnika.class.getName()).log(Level.SEVERE, null, ex);
        }
        return korisnici;
    }

    public static boolean postojiLiKorisnik(String ime, String prezime) {
        inicijaliziraj();
        boolean postojiKorisnik = false;
        String upitKorisnik = "SELECT * FROM korisnici WHERE ime=? AND prezime=?";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnikBaze, lozinkaBaze)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitKorisnik);
            preparedStmt.setString(1, ime);
            preparedStmt.setString(2, prezime);
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                postojiKorisnik = true;
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(KontrolaKorisnika.class.getName()).log(Level.SEVERE, null, ex);
        }
        return postojiKorisnik;
    }

    static String dodajNovogKorisnika(String korime, String lozinka, String prezime, String ime) {
        inicijaliziraj();
        korime = korime.substring(0, Math.min(korime.length(), 9));
        String email = kreirajEmail(korime);
        Korisnik korisnik = new Korisnik(korime, ime, prezime, lozinka, email);
        String upitKorisnik = "INSERT INTO korisnici (kor_ime, ime, prezime, lozinka, email_adresa, datum_promjene)"
                + " VALUES (?, ?, ?, ?, ?,  NOW())";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnikBaze, lozinkaBaze)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitKorisnik);
            preparedStmt.setString(1, korisnik.getKorime());
            preparedStmt.setString(2, korisnik.getIme());
            preparedStmt.setString(3, korisnik.getPrezime());
            preparedStmt.setString(4, korisnik.getLozinka());
            preparedStmt.setString(5, korisnik.getEmail());
            preparedStmt.execute();
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(KontrolaKorisnika.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "Vase korisnicko ime je: " + korime + ", a vasa lozinka je: 123456";
    }

//    private static String kreirajKorime(String ime, String prezime) {
//        if (prezime.length() >= 9) {
//            prezime = prezime.substring(0, 9);
//        }
//
//        String korime = ime.substring(0, 1) + prezime;
//        korime = korime.toLowerCase();
//
//        return korime;
//    }
    private static String kreirajEmail(String korime) {
        String email = korime + "@foi.hr";
        return email;
    }

    static boolean postojiLiKorisnik(String korime) {
        inicijaliziraj();
        boolean postojiKorisnik = false;
        String upitKorisnik = "SELECT * FROM korisnici WHERE kor_ime=?";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnikBaze, lozinkaBaze)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitKorisnik);
            preparedStmt.setString(1, korime);
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                postojiKorisnik = true;
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(KontrolaKorisnika.class.getName()).log(Level.SEVERE, null, ex);
        }
        return postojiKorisnik;
    }

    static void azurirajKorisnika(String korime, String prezime, String ime) {
        inicijaliziraj();
        String upitKorisnik = "UPDATE korisnici SET ime=?, prezime=?, datum_promjene= NOW() WHERE kor_ime=?";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnikBaze, lozinkaBaze)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitKorisnik);
            preparedStmt.setString(1, ime);
            preparedStmt.setString(2, prezime);
            preparedStmt.setString(3, korime);
            preparedStmt.execute();
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(KontrolaKorisnika.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static Korisnik vratiKorisnika(String korime) {
        inicijaliziraj();
        Korisnik korisnik = new Korisnik();
        String upitKorisnik = "SELECT * FROM korisnici WHERE kor_ime=?";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnikBaze, lozinkaBaze)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitKorisnik);
            preparedStmt.setString(1, korime);
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                korisnik.setId(odgovor.getInt("ID"));
                korisnik.setVrsta(odgovor.getString("VRSTA"));
                korisnik.setIme(odgovor.getString("IME"));
                korisnik.setPrezime(odgovor.getString("PREZIME"));
                korisnik.setKorime(odgovor.getString("KOR_IME"));
                korisnik.setEmail(odgovor.getString("EMAIL_ADRESA"));
                korisnik.setDatumKreiranja(odgovor.getDate("DATUM_KREIRANJA"));
                korisnik.setDatumPromjene(odgovor.getDate("DATUM_PROMJENE"));
                korisnik.setLozinka("skriveno");
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(KontrolaKorisnika.class.getName()).log(Level.SEVERE, null, ex);
        }
        return korisnik;
    }

}
