package org.foi.nwtis.globabic.web.dretve;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.web.kontrole.KontrolaKomande;

public class RadnaDretva extends Thread {

    private Socket socket;
    private Konfiguracija konfiguracija;
    private String komanda;
    private String imeDretve;
    private long startTime;

    RadnaDretva(Socket socket, String imeDretve, Konfiguracija konfiguracija, String komanda) {
        super(imeDretve);
        this.socket = socket;
        this.imeDretve = imeDretve;
        this.konfiguracija = konfiguracija;
        this.komanda = komanda;
    }

    @Override
    public void interrupt() {
        super.interrupt();
    }

    @Override
    public synchronized void start() {
        super.start(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void run() {
        System.err.println("POKRENUTA RADNA DRETVA");
        startTime = System.currentTimeMillis();
        System.out.println("RADNA DRETVA: " + imeDretve + " komanda: " + komanda);

        String odgovor = KontrolaKomande.izvrsiKomandu(komanda);
        System.out.println("RADNA DRETVA: " + imeDretve + " odgovor: " + odgovor);
        posaljiOdgovor(odgovor);

    }

    private void posaljiOdgovor(String odgovor) {
        try {
            OutputStream os;
            os = socket.getOutputStream();
            os.write(odgovor.getBytes());
            os.flush();
            socket.shutdownOutput();
        } catch (IOException ex) {
            Logger.getLogger(RadnaDretva.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Konfiguracija getKonfiguracija() {
        return konfiguracija;
    }

    public void setKonfiguracija(Konfiguracija konfiguracijaiguracija) {
        this.konfiguracija = konfiguracijaiguracija;
    }

}
