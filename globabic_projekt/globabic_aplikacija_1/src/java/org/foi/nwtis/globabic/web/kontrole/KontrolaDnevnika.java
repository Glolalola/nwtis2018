/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.kontrole;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.foi.nwtis.globabic.konfiguracije.bp.BP_Konfiguracija;
import org.foi.nwtis.globabic.web.podaci.Dnevnik;
import org.foi.nwtis.globabic.web.slusaci.SlusacAplikacije;

/**
 *
 * @author Gloria Babić
 */
public class KontrolaDnevnika {

    private static String url;
    private static String korisnikBaze;
    private static String lozinkaBaze;
    private static String driver;
    private static final ServletContext sc = SlusacAplikacije.getServletContext();

    private static void inicijaliziraj() {
        BP_Konfiguracija bpk = (BP_Konfiguracija) sc.getAttribute("BP_Konfig");
        if (bpk == null) {
            System.err.println("Problem s konfiguracijom!");
            return;
        }
        url = bpk.getServerDatabase() + bpk.getAdminDatabase();
        korisnikBaze = bpk.getAdminUsername();
        lozinkaBaze = bpk.getAdminPassword();
        driver = bpk.getDriverDatabase();

    }

    public static void spremiDnevnik(Dnevnik dnevnik) {
        inicijaliziraj();
        String upitDnevnik = "INSERT INTO dnevnik (KORISNIK, URL, IPADRESA, TRAJANJE, STATUS)"
                + " VALUES (?, ?, ?, ?, ?)";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnikBaze, lozinkaBaze)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitDnevnik);
            preparedStmt.setString(1, dnevnik.getKorisnik());
            preparedStmt.setString(2, dnevnik.getUrl());
            preparedStmt.setString(3, dnevnik.getIpAdresa());
            preparedStmt.setInt(4, dnevnik.getTrajanje());
            preparedStmt.setInt(5, dnevnik.getStatus());
            preparedStmt.execute();
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(KontrolaDnevnika.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
