/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.kontrole;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.foi.nwtis.globabic.web.dretve.DretvaServera;

/**
 *
 * @author Gloria Babić
 */
public class KontrolaKomande {

    public static final String regexServerKomande = "^KORISNIK ([A-Za-z0-9_,-]{3,10}); LOZINKA ([A-Za-z0-9_,#,!,-]{3,10});(.*)";
    private static final String regexServer = "^KORISNIK ([A-Za-z0-9_,-]{3,10}); LOZINKA ([A-Za-z0-9_,#,!,-]{3,10}); (PAUZA|KRENI|PASIVNO|AKTIVNO|STANI|STANJE|LISTAJ);$";
    private static final String regexKorisnik = "^KORISNIK ([A-Za-z0-9_,-]{3,10}); LOZINKA ([A-Za-z0-9_,#,!,-]{3,10}); (DODAJ|AZURIRAJ) ([A-Za-z0-9_,-]{3,10}) ([A-Za-z0-9_,-]{3,10});$";
    private static final String regexKorisnikPreuzmi = "^KORISNIK ([A-Za-z0-9_,-]{3,10}); LOZINKA ([A-Za-z0-9_,#,!,-]{3,10}); PREUZMI ([A-Za-z0-9_,-]{3,10});$";
    private static final String regexGrupa = "^KORISNIK ([A-Za-z0-9_,-]{3,10}); LOZINKA ([A-Za-z0-9_,#,!,-]{3,10}); GRUPA (DODAJ|PREKID|KRENI|PAUZA|STANJE);$";

    public static String procitajKomandu(Socket socket) {
        StringBuffer buffer = new StringBuffer();

        try {
            InputStream is = socket.getInputStream();

            while (true) {
                int znak = is.read();
                if (znak == -1) {
                    break;
                }
                buffer.append((char) znak);
            }
        } catch (IOException ex) {
            Logger.getLogger(KontrolaKomande.class.getName()).log(Level.SEVERE, null, ex);
        }
        return buffer.toString();
    }

    public static void odgovoriNaKomandu(String odgovor, Socket socket) {
        try {
            OutputStream os;
            os = socket.getOutputStream();
            os.write(odgovor.getBytes());
            os.flush();
            socket.shutdownOutput();
        } catch (IOException ex) {
            Logger.getLogger(KontrolaKomande.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String izvrsiKomandu(String komanda) {
        String odgovor = "";

        Matcher serverMatcher = provjeriKomandu(komanda, regexServer);
        Matcher korisnikMatcher = provjeriKomandu(komanda, regexKorisnik);
        Matcher korisnikPreuzmiMatcher = provjeriKomandu(komanda, regexKorisnikPreuzmi);
        Matcher grupaMatcher = provjeriKomandu(komanda, regexGrupa);

        if (serverMatcher.matches() == true) {
            odgovor = KontrolaServer.pozoviAkcijuServera(serverMatcher.group(3));
        } else if (korisnikMatcher.matches() == true) {
            odgovor = KontrolaServer.pozoviAkcijuKorisnik(korisnikMatcher.group(1),korisnikMatcher.group(2), korisnikMatcher.group(3), korisnikMatcher.group(5), korisnikMatcher.group(4));
        }  else if (korisnikPreuzmiMatcher.matches() == true) {
            odgovor = KontrolaServer.pozoviAkcijuKorisnikPreuzmi(korisnikPreuzmiMatcher.group(3));
        }else if (grupaMatcher.matches() == true) {
            if (DretvaServera.serverPrimaKomande == false) {
                return "Primanje komandi je pauzirano!";
            }
            odgovor = KontrolaGrupa.pozoviAkcijuGrupe(grupaMatcher.group(3));
        } else {
            odgovor = "Pogrešna komanda.";
        }

        return odgovor;
    }

    public static Matcher provjeriKomandu(String komanda, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(komanda);

        return matcher;
    }

}
