package org.foi.nwtis.globabic.rest.serveri;

import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.servlet.ServletContext;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.konfiguracije.bp.BP_Konfiguracija;
import org.foi.nwtis.globabic.rest.klijenti.GMKlijent;
import org.foi.nwtis.globabic.web.kontrole.KontrolaDnevnika;
import org.foi.nwtis.globabic.web.kontrole.KontrolaKorisnika;
import org.foi.nwtis.globabic.web.podaci.Dnevnik;
import org.foi.nwtis.globabic.web.podaci.Lokacija;
import org.foi.nwtis.globabic.web.podaci.Parkiraliste;
import org.foi.nwtis.globabic.web.slusaci.SlusacAplikacije;
import org.foi.nwtis.globabic.ws.klijenti.ParkiralistaWSKlijent;

/**
 * Implementira dva REST web servisa za parkirališta parkiralištima:
 *
 *
 * @author Gloria Babić
 */
@Path("parkiraliste")
public class ParkiralisteREST {

    @Context
    private UriInfo context;

   
    private String url;
    private String korisnik;
    private String lozinka;
    private String driver;
    private String gmapikey;
    private ServletContext sc = null;
    private Konfiguracija konf;

    /**
     * Creates a new instance of MeteoREST
     */
    public ParkiralisteREST() {
    }

    /**
     * Retrieves representation of an instance of
     * org.foi.nwtis.globabic.rest.serveri.ParkiralisteREST
     *
     * vraća popis svih parkirališta, njihovih adesa i geo lokacija u
     * application/json formatu. Struktura odgovora je u sljedeća: {"odgovor":
     * [{...},{...}...], "status": "OK" | "ERR",
     * <ako je "ERR" onda se  dodaje "poruka": poruka>}.
     *
     * @param korime
     * @param lozinka
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@HeaderParam("korime") String korime, @HeaderParam("lozinka") String lozinka) {
        Long start = System.currentTimeMillis();
        inicijaliziraj();
        if (ParkiralistaWSKlijent.dajSvaParkiralistaGrupe(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword")) == null || ParkiralistaWSKlijent.dajSvaParkiralistaGrupe(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword")).isEmpty()) {
            sinkroniziraj();
        }

        String odgovor;
        if (KontrolaKorisnika.autenticirajKorisnika(korime, lozinka)) {
            odgovor = dohvatiParkiralista();
            if (odgovor.contains("OK")) {
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 1));
            }
        } else {
            KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));
            odgovor = (Json.createObjectBuilder()
                    .add("odgovor", "Nije prošla autentikacija.")
                    .build()).toString();
        }
        return odgovor;
    }

    /**
     * dodaje parkiralište (šalje se naziv i adresa koja služi za preuzimanje
     * geo lokacije) Šalju se podaci u application/json formatu. Vraća odgovor u
     * application/json formatu. Struktura odgovora je u sljedeća: {"odgovor":
     * [], "status": "OK" | "ERR",
     * <ako je "ERR" onda se  dodaje "poruka": poruka>}.
     *
     * @param korime
     * @param lozinka
     * @param podaci
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String postJson(@HeaderParam("korime") String korime, @HeaderParam("lozinka") String lozinka, String podaci) {
        Long start = System.currentTimeMillis();
        Gson gson = new Gson();
        JsonObject jsonOdgovor;
        if (KontrolaKorisnika.autenticirajKorisnika(korime, lozinka)) {
            org.foi.nwtis.globabic.ws.klijenti.Parkiraliste parkiralistezaWS = gson.fromJson(podaci, org.foi.nwtis.globabic.ws.klijenti.Parkiraliste.class);
            if ((spremiUBazu(parkiralistezaWS.getNaziv(), parkiralistezaWS.getAdresa(), parkiralistezaWS.getKapacitet())) && (spremiUWS(parkiralistezaWS.getNaziv(), parkiralistezaWS.getAdresa(), parkiralistezaWS.getKapacitet()))) {
                jsonOdgovor = (Json.createObjectBuilder()
                        .add("odgovor", "")
                        .add("status", "OK")
                        .build());
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 1));

            } else {
                jsonOdgovor = (Json.createObjectBuilder()
                        .add("odgovor", "")
                        .add("status", "ERR")
                        .add("poruka", "Postoji parkiralište s tim imenom!")
                        .build());
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));

            }
        } else {
            KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", "Nije prošla autentikacija.")
                    .build());

        }
        return jsonOdgovor.toString();
    }

    /**
     * Na bazi putanje {id} za izabrano parkiralište ažurira podatke (šalju se
     * naziv i adresa, koja služi za preuzimanje geo lokacije). Šalju se podaci
     * u application/json formatu. Vraća podatke u application/json formatu.
     * Struktura odgovora je u sljedeća: {"odgovor": [], "status": "OK" | "ERR",
     * <ako je "ERR" onda se  dodaje "poruka": poruka>}.
     *
     * @return
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String putJson() {

        JsonObject jsonOdgovor;

        jsonOdgovor = (Json.createObjectBuilder()
                .add("odgovor", "")
                .add("status", "ERR")
                .add("poruka", "Nedozvoljena putanja!")
                .build());

        return jsonOdgovor.toString();
    }

    /**
     * Vraća grešku. Šalju se podaci u application/json formatu. Vraća odgovor u
     * application/json formatu. Struktura odgovora je u sljedeća: {"odgovor":
     * [], "status": "OK" | "ERR",
     * <ako je "ERR" onda se  dodaje "poruka": poruka>}.
     *
     * @param id
     * @param podaci
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public String postJson(@PathParam("id") String id, String podaci) {

        JsonObject jsonOdgovor;

        jsonOdgovor = (Json.createObjectBuilder()
                .add("odgovor", "")
                .add("status", "ERR")
                .add("poruka", "Nedozvoljena putanja!")
                .build());

        return jsonOdgovor.toString();
    }

    /**
     * Vraća grešku. Šalju se podaci u application/json formatu. Vraća odgovor u
     * application/json formatu. Struktura odgovora je u sljedeća: {"odgovor":
     * [], "status": "OK" | "ERR",
     * <ako je "ERR" onda se  dodaje "poruka": poruka>}.
     *
     * @return
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteJson() {

        JsonObject jsonOdgovor;

        jsonOdgovor = (Json.createObjectBuilder()
                .add("odgovor", "")
                .add("status", "ERR")
                .add("poruka", "Nedozvoljena putanja!")
                .build());

        return jsonOdgovor.toString();
    }

    /**
     * Na bazi putanje {id} za izabrano parkiralište vraća podatke. Vraća
     * podatke u application/json formatu. Struktura odgovora je u sljedeća:
     * {"odgovor": [{...}], "status": "OK" | "ERR",
     * <ako je "ERR" onda se  dodaje "poruka": poruka>}.
     *
     * @param id
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public String getJson(@HeaderParam("korime") String korime, @HeaderParam("lozinka") String lozinka, @PathParam("id") String id) {
        Long start = System.currentTimeMillis();
        JsonObject jsonOdgovor;
        if (KontrolaKorisnika.autenticirajKorisnika(korime, lozinka)) {
            if (provjeriParkiralisteUWS(Integer.valueOf(id))) {

                jsonOdgovor = dohvatiParkiralisteZaID(id);
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 1));

            } else {
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));

                jsonOdgovor = (Json.createObjectBuilder()
                        .add("odgovor", "")
                        .add("status", "ERR")
                        .add("poruka", "Ne postoji parkiraliste s tim ID-jem!")
                        .build());
            }
        } else {
            KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", "Nije prošla autentikacija.")
                    .build());

        }
        return jsonOdgovor.toString();
    }

    /**
     *
     * Na bazi putanje {id} za izabrano parkiralište ažurira podatke (šalju se
     * naziv i adresa, koja služi za preuzimanje geo lokacije). Šalju se podaci
     * u application/json formatu. Vraća podatke u application/json formatu.
     * Struktura odgovora je u sljedeća: {"odgovor": [], "status": "OK" | "ERR",
     * <ako je "ERR" onda se  dodaje "poruka": poruka>}.
     *
     * @param korime
     * @param lozinka
     * @param id
     * @param podaci
     * @return
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public String putJson(@HeaderParam("korime") String korime, @HeaderParam("lozinka") String lozinka, @PathParam("id") String id, String podaci) {
        Long start = System.currentTimeMillis();
        Gson gson = new Gson();
        JsonObject jsonOdgovor;
        if (KontrolaKorisnika.autenticirajKorisnika(korime, lozinka)) {
            org.foi.nwtis.globabic.ws.klijenti.Parkiraliste parkiralistezaWS = gson.fromJson(podaci, org.foi.nwtis.globabic.ws.klijenti.Parkiraliste.class);

            if (provjeriParkiralisteUWS(Integer.parseInt(id))) {
                jsonOdgovor = azurirajParkiralisteZaID(id, parkiralistezaWS);
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 1));

            } else {
                jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                        .add("odgovor", "")
                        .add("status", "ERR")
                        .add("poruka", "Ne postoji parkiraliste s tim ID-jem!")
                        .build());
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));

            }
        } else {
            KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", "Nije prošla autentikacija.")
                    .build());
        }
        return jsonOdgovor.toString();

    }

    /**
     * a bazi putanje {id} briše izabrano parkiralište. Vraća podatke u
     * application/json formatu. Struktura odgovora je u sljedeća: {"odgovor":
     * [], "status": "OK" | "ERR",
     * <ako je "ERR" onda se  dodaje "poruka": poruka>}.
     *
     * Parkiralište se ne briše ako postoje meteo podaci za njega.
     *
     * @param korime
     * @param lozinka
     * @param id
     * @return
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public String deleteJson(@HeaderParam("korime") String korime, @HeaderParam("lozinka") String lozinka, @PathParam("id") String id) {
        Long start = System.currentTimeMillis();
        JsonObject jsonOdgovor;
        if (KontrolaKorisnika.autenticirajKorisnika(korime, lozinka)) {
            if (provjeriParkiralisteUWS(Integer.parseInt(id)) && provjeriParkiralisteUBazi(Integer.parseInt(id))) {
                inicijaliziraj();
                if(ParkiralistaWSKlijent.dajSvaVozilaParkiralistaGrupe(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword"), Integer.parseInt(id)).isEmpty()){
                    jsonOdgovor = obrisiParkiraliste(id);
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 1));

                }else{
                    jsonOdgovor = (Json.createObjectBuilder()
                        .add("odgovor", "")
                        .add("status", "ERR")
                        .add("poruka", "Ne može se obrisati parkiralište koje ima vozila!")
                        .build());
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));

                }
                
            } else {
                jsonOdgovor = (Json.createObjectBuilder()
                        .add("odgovor", "")
                        .add("status", "ERR")
                        .add("poruka", "Ne postoji parkiraliste s tim ID-jem!")
                        .build());
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));

            }
        } else {
            KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", "Nije prošla autentikacija.")
                    .build());
        }
        return jsonOdgovor.toString();

    }

    /**
     *
     * @param korime
     * @param lozinka
     * @param id
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/vozila/")
    public String getJsonCars(@HeaderParam("korime") String korime, @HeaderParam("lozinka") String lozinka, @PathParam("id") String id) {
        Long start = System.currentTimeMillis();

        JsonObject jsonOdgovor;
        if (KontrolaKorisnika.autenticirajKorisnika(korime, lozinka)) {
            if (provjeriParkiralisteUWS(Integer.valueOf(id))) {

                jsonOdgovor = dohvatiVozilaZaID(id);
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 1));

            } else {
                jsonOdgovor = (Json.createObjectBuilder()
                        .add("odgovor", "")
                        .add("status", "ERR")
                        .add("poruka", "Ne postoji parkiraliste s tim ID-jem!")
                        .build());
                KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));

            }
        } else {
            KontrolaDnevnika.spremiDnevnik(new Dnevnik(korime, context.getRequestUri().toString(), (int) (System.currentTimeMillis() - start), 0));
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", "Nije prošla autentikacija.")
                    .build());
        }
        return jsonOdgovor.toString();
    }

    /**
     * Inicijalizira podatke potrebne za rad sa bazom. Dohvaća podatke iz
     * konfiguracija.
     */
    private void inicijaliziraj() {
        sc = SlusacAplikacije.getServletContext();
        konf = (Konfiguracija) sc.getAttribute("konfiguracija");

        BP_Konfiguracija bpk = (BP_Konfiguracija) sc.getAttribute("BP_Konfig");
        if (bpk == null) {
            System.err.println("Problem s konfiguracijom!");
            return;
        }
        url = bpk.getServerDatabase() + bpk.getAdminDatabase();
        korisnik = bpk.getAdminUsername();
        lozinka = bpk.getAdminPassword();
        driver = bpk.getDriverDatabase();
        gmapikey = konf.dajPostavku("gmapikey");
    }

    /**
     * Dohvaća sva parkirališta iz baze za GET metodu. Oblikuje json poruku koju
     * treba vratiti.
     *
     * @return vraća json objekt u stringu
     */
    public String dohvatiParkiralista() {
        boolean ok = true;
        String poruka = null;
        JsonObject jsonOdgovor;
        JsonArray jsonParkiralista = dohvatiParkiralistaSaWS().build();
        if (ok) {
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", jsonParkiralista)
                    .add("status", "OK")
                    .build());
        } else {
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", jsonParkiralista)
                    .add("status", "ERR")
                    .add("poruka", poruka)
                    .build());
        }
        return jsonOdgovor.toString();
    }

    /**
     * Sprema parkiralište u bazu ako ne postoji već parkiralište s tim imenom.
     *
     * @param naziv
     * @param adresa
     * @return
     */
    public boolean spremiUWS(String naziv, String adresa, int kapacitet) {
        inicijaliziraj();
        int noviID = dohvatiID();
        System.err.println("Dohvati id? " + noviID);
        boolean dodano = ParkiralistaWSKlijent.dodajNovoParkiralisteGrupi(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword"), noviID, naziv, adresa, kapacitet);
        System.out.println("Dodanjo u WS, " + dodano);
        return dodano;
    }

    /**
     * Sprema parkiralište u bazu ako ne postoji već parkiralište s tim imenom.
     *
     * @param naziv
     * @param adresa
     * @param kapacitet
     * @return
     */
    public boolean spremiUBazu(String naziv, String adresa, int kapacitet) {
        inicijaliziraj();
        GMKlijent gmk = new GMKlijent(gmapikey);
        Lokacija lokacija = gmk.getGeoLocation(adresa);
        boolean dodano = false;
        String upitParkiralista = "INSERT INTO PARKIRALISTA (NAZIV, ADRESA, KAPACITET, LATITUDE, LONGITUDE) values(?,?,?,?,?)";
        if (!provjeriParkiraliste(naziv)) {
            try {
                Class.forName(driver);
            } catch (ClassNotFoundException ex) {
                System.out.println("Greska s driverom " + ex);
            }
            try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
                PreparedStatement preparedStmt = veza.prepareStatement(upitParkiralista);
                preparedStmt.setString(1, naziv);
                preparedStmt.setString(2, adresa);
                preparedStmt.setInt(3, kapacitet);
                preparedStmt.setFloat(4, Float.parseFloat(lokacija.getLatitude()));
                preparedStmt.setFloat(5, Float.parseFloat(lokacija.getLongitude()));
                preparedStmt.execute();
                veza.close();
                dodano = true;
            } catch (SQLException ex) {
                Logger.getLogger(ParkiralisteREST.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("dodanou bazu : " + dodano);
        return dodano;
    }

    /**
     * Provjerava postoji li parkiralište s danim imenom
     *
     * @param naziv
     * @return
     */
    public boolean provjeriParkiraliste(String naziv) {
        String upitParkiralista = "SELECT ID FROM PARKIRALISTA WHERE NAZIV = ?";
        boolean ima = false;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitParkiralista);
            preparedStmt.setString(1, naziv);
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                ima = true;
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(ParkiralisteREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ima;
    }

    /**
     * Provjerava postoji li parkiralište s danim id-jem
     *
     * @param id
     * @return
     */
    public boolean provjeriParkiralisteUWS(int id) {
        boolean ima = false;
        inicijaliziraj();
        List<org.foi.nwtis.globabic.ws.klijenti.Parkiraliste> svaParkiralista = ParkiralistaWSKlijent.dajSvaParkiralistaGrupe(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword"));

        for (org.foi.nwtis.globabic.ws.klijenti.Parkiraliste parkiraliste1 : svaParkiralista) {
            if (parkiraliste1.getId() == id) {
                ima = true;
            }
        }
        return ima;
    }

    public boolean provjeriParkiralisteUBazi(int id) {
        inicijaliziraj();
        String upitParkiralista = "SELECT * FROM PARKIRALISTA WHERE ID = ?";
        boolean ima = false;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitParkiralista);
            preparedStmt.setInt(1, id);
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                ima = true;
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(ParkiralisteREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ima;
    }

    /**
     * Dohvaća podatke za parkiralište s danim id-jem i oblikuje potreban json
     * odgovor.
     *
     * @param id
     * @return
     */
    public JsonObject dohvatiParkiralisteZaID(String id) {
        boolean ok = true;
        int idParkiralista = Integer.valueOf(id);
        String poruka = null;
        inicijaliziraj();
        JsonObject jsonOdgovor;
        JsonArray jsonParkiralista = dohvatiParkiralisteSaWS(idParkiralista).build();
        if (ok) {
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", jsonParkiralista)
                    .add("status", "OK")
                    .build());
        } else {
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", jsonParkiralista)
                    .add("status", "ERR")
                    .add("poruka", poruka)
                    .build());
        }
        return jsonOdgovor;
    }

    /**
     * Ažurira podatke za parkiralište s danim id-jem na osnovu danih podataka i
     * oblikuje potreban json odgovor.
     *
     * @param id
     * @param parkiraliste
     * @return
     */
    public JsonObject azurirajParkiralisteZaID(String id, org.foi.nwtis.globabic.ws.klijenti.Parkiraliste parkiraliste) {
        String poruka = "";
        inicijaliziraj();
        parkiraliste.setGeoloc(dohvatiLokaciju(parkiraliste.getAdresa()));
        parkiraliste.setId(Integer.parseInt(id));
        boolean dodano = false;
        String upitParkiralista = "UPDATE PARKIRALISTA SET NAZIV = ?,ADRESA = ?,LATITUDE = ?,LONGITUDE = ?, KAPACITET = ?, STATUS = ? WHERE ID = ?";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom");
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitParkiralista);
            preparedStmt.setString(1, parkiraliste.getNaziv());
            preparedStmt.setString(2, parkiraliste.getAdresa());
            preparedStmt.setFloat(3, Float.parseFloat(parkiraliste.getGeoloc().getLatitude()));
            preparedStmt.setFloat(4, Float.parseFloat(parkiraliste.getGeoloc().getLongitude()));
            preparedStmt.setInt(5, parkiraliste.getKapacitet());
            preparedStmt.setString(6, parkiraliste.getStatus().value());
            preparedStmt.setInt(7, Integer.valueOf(id));
            System.err.println("OVO JE SQL: " + preparedStmt.toString());
            preparedStmt.execute();
            veza.close();
            dodano = true;
        } catch (SQLException ex) {
            Logger.getLogger(ParkiralisteREST.class.getName()).log(Level.SEVERE, null, ex);
            poruka = ParkiralisteREST.class.getName() + " " + ex;
        }
        if (!azurirajParkiralisteZaIDUWS(parkiraliste)) {
            poruka = "Parkiraliste nije ažurirano u WS!";
        }
        return oblikujJson(dodano, poruka);
    }

    /**
     * oblikuje potreban json odgovor ovisno o proslijeđenj bool vrijednosti
     *
     * @param dodano
     * @param poruka
     * @return
     */
    public JsonObject oblikujJson(boolean dodano, String poruka) {
        JsonObject jsonOdgovor;
        if (dodano) {
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", "")
                    .add("status", "OK")
                    .build());
        } else {
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", "")
                    .add("status", "ERR")
                    .add("poruka", poruka)
                    .build());
        }
        return jsonOdgovor;
    }

    /**
     * Briše parkiralište s danim id-jem ako nema meta podataka za to
     * parkirališe
     *
     * @param id
     * @return
     */
    public JsonObject obrisiParkiraliste(String id) {
        String poruka = "";
        boolean obrisano = false;
        String upitParkiralista = "DELETE FROM PARKIRALISTA WHERE ID =?";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitParkiralista);
            preparedStmt.setInt(1, Integer.valueOf(id));
            preparedStmt.execute();
            veza.close();
            obrisano = true;
        } catch (SQLException ex) {
            Logger.getLogger(ParkiralisteREST.class.getName()).log(Level.SEVERE, null, ex);
            poruka = "Ne može se obrisati parkiralište koje ima meteo podatke!";

        }
        return oblikujJson(obrisano && obrisiUWS(Integer.parseInt(id)), poruka);
    }

    /**
     * dohvaća parkiralište iz baze
     *
     * @return
     */
    public JsonArrayBuilder dohvatiParkiralistaSaWS() {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        List<org.foi.nwtis.globabic.ws.klijenti.Parkiraliste> svaParkiralista = ParkiralistaWSKlijent.dajSvaParkiralistaGrupe(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword"));

        for (org.foi.nwtis.globabic.ws.klijenti.Parkiraliste parkiraliste1 : svaParkiralista) {

            builder.add(Json.createObjectBuilder()
                    .add("id", parkiraliste1.getId())
                    .add("naziv", parkiraliste1.getNaziv())
                    .add("adresa", parkiraliste1.getAdresa())
                    .add("kapacitet", parkiraliste1.getKapacitet())
                    .add("latitude", parkiraliste1.getGeoloc().getLatitude())
                    .add("longitude", parkiraliste1.getGeoloc().getLongitude())
                    .add("status", parkiraliste1.getStatus().toString()));
        }

        return builder;
    }

    private JsonArrayBuilder dohvatiParkiralisteSaWS(int idParkiralista) {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        inicijaliziraj();
        List<org.foi.nwtis.globabic.ws.klijenti.Parkiraliste> svaParkiralista = ParkiralistaWSKlijent.dajSvaParkiralistaGrupe(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword"));

        for (org.foi.nwtis.globabic.ws.klijenti.Parkiraliste parkiraliste1 : svaParkiralista) {
            if (parkiraliste1.getId() == idParkiralista) {
                System.err.println("PARKIRALISTE: " + parkiraliste1.getNaziv());
                builder.add(Json.createObjectBuilder()
                        .add("id", parkiraliste1.getId())
                        .add("naziv", parkiraliste1.getNaziv())
                        .add("adresa", parkiraliste1.getAdresa())
                        .add("kapacitet", parkiraliste1.getKapacitet())
                        .add("latitude", parkiraliste1.getGeoloc().getLatitude())
                        .add("longitude", parkiraliste1.getGeoloc().getLongitude())
                        .add("status", parkiraliste1.getStatus().toString()));
            }
        }

        return builder;
    }

    private int dohvatiID() {
        inicijaliziraj();
        int id = 0;
        String upitID = "SELECT * FROM PARKIRALISTA ORDER BY ID";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(upitID);
            preparedStmt.execute();
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                id = odgovor.getInt("ID");
                System.err.println("id = " + id);
            }
            veza.close();

        } catch (SQLException ex) {
            Logger.getLogger(ParkiralisteREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

    private boolean obrisiUWS(int id) {
        return ParkiralistaWSKlijent.obrisiParkiralisteGrupe(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword"), id);
    }

    private boolean azurirajParkiralisteZaIDUWS(org.foi.nwtis.globabic.ws.klijenti.Parkiraliste parkiraliste) {
        try {
            obrisiUWS(parkiraliste.getId());
            ParkiralistaWSKlijent.dodajParkiralisteGrupi(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword"), parkiraliste);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private org.foi.nwtis.globabic.ws.klijenti.Lokacija dohvatiLokaciju(String adresa) {
        GMKlijent gmk = new GMKlijent(gmapikey);
        Lokacija l = new Lokacija();
        l = gmk.getGeoLocation(adresa);
        org.foi.nwtis.globabic.ws.klijenti.Lokacija lokacija = new org.foi.nwtis.globabic.ws.klijenti.Lokacija();
        lokacija.setLatitude(l.getLatitude());
        lokacija.setLongitude(l.getLongitude());
        return lokacija;
    }

    private JsonObject dohvatiVozilaZaID(String id) {
        boolean ok = true;
        String poruka = null;
        inicijaliziraj();
        JsonObject jsonOdgovor;
        JsonArray jsonVozila = dohvatiVozilaSaWS(Integer.parseInt(id)).build();
        if (ok) {
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", jsonVozila)
                    .add("status", "OK")
                    .build());
        } else {
            jsonOdgovor = (Json.createObjectBuilder()
                    .add("odgovor", jsonVozila)
                    .add("status", "ERR")
                    .add("poruka", poruka)
                    .build());
        }
        return jsonOdgovor;
    }

    public JsonArrayBuilder dohvatiVozilaSaWS(int idParkiralista) {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        List<org.foi.nwtis.globabic.ws.klijenti.Vozilo> vozila = ParkiralistaWSKlijent.dajSvaVozilaParkiralistaGrupe(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword"), idParkiralista);

        for (org.foi.nwtis.globabic.ws.klijenti.Vozilo vozilo : vozila) {

            builder.add(Json.createObjectBuilder()
                    .add("akcija", vozilo.getAkcija().value())
                    .add("parkiraliste", vozilo.getParkiraliste())
                    .add("registracija", vozilo.getRegistracija())
            );
        }

        return builder;
    }

    private void sinkroniziraj() {
        inicijaliziraj();
        System.err.println("tu1");
        ArrayList<Parkiraliste> parkiralistaIzBaze = new ArrayList<>();
        parkiralistaIzBaze = dohvatiParkiralistaIzBaze();
        System.err.println("tu2");
        ParkiralistaWSKlijent.obrisiSvaParkiralistaGrupe(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword"));
        System.err.println("tu3:" + parkiralistaIzBaze.size());
        for (int i = 0; i < parkiralistaIzBaze.size(); i++) {
            System.err.println("tu4");
            ParkiralistaWSKlijent.dodajParkiralisteGrupi(konf.dajPostavku("subversionUsername"), konf.dajPostavku("subversionPassword"), pretvoriParkiraliste(parkiralistaIzBaze.get(i)));

            System.err.println("tu5");
        }

    }

    public ArrayList<Parkiraliste> dohvatiParkiralistaIzBaze() {
        ArrayList<Parkiraliste> parkiralista = new ArrayList<>();
        String query = "SELECT * FROM PARKIRALISTA";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            System.out.println("Greska s driverom " + ex);
        }
        try (Connection veza = DriverManager.getConnection(url, korisnik, lozinka)) {
            PreparedStatement preparedStmt = veza.prepareStatement(query);
            preparedStmt.execute();
            ResultSet odgovor = preparedStmt.executeQuery();
            while (odgovor.next()) {
                Parkiraliste p = new Parkiraliste();
                Lokacija l = new Lokacija();
                p.setId(odgovor.getInt("ID"));
                p.setNaziv(odgovor.getString("NAZIV"));
                p.setAdresa(odgovor.getString("ADRESA"));
                l.setLatitude(odgovor.getString("LATITUDE"));
                l.setLongitude(odgovor.getString("LONGITUDE"));
                p.setGeoloc(l);
                p.setKapacitet(odgovor.getInt("KAPACITET"));
                p.setStatus(pretvoriStatus(odgovor.getString("STATUS")));
                parkiralista.add(p);
            }
            veza.close();
        } catch (SQLException ex) {
            Logger.getLogger(ParkiralisteREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return parkiralista;
    }

    private org.foi.nwtis.globabic.ws.klijenti.StatusParkiralista pretvoriStatus(String status) {
        org.foi.nwtis.globabic.ws.klijenti.StatusParkiralista statusParkiralista;
        switch (status) {
            case "PASIVAN":
                statusParkiralista = org.foi.nwtis.globabic.ws.klijenti.StatusParkiralista.PASIVAN;
                break;
            case "AKTIVAN":
                statusParkiralista = org.foi.nwtis.globabic.ws.klijenti.StatusParkiralista.AKTIVAN;
                break;
            case "BLOKIRAN":
                statusParkiralista = org.foi.nwtis.globabic.ws.klijenti.StatusParkiralista.BLOKIRAN;
                break;
            case "NEPOSTOJI":
                statusParkiralista = org.foi.nwtis.globabic.ws.klijenti.StatusParkiralista.NEPOSTOJI;
                break;
            default:
                statusParkiralista = org.foi.nwtis.globabic.ws.klijenti.StatusParkiralista.NEPOSTOJI;
        }
        return statusParkiralista;
    }

    private org.foi.nwtis.globabic.ws.klijenti.Parkiraliste pretvoriParkiraliste(Parkiraliste p) {
        org.foi.nwtis.globabic.ws.klijenti.Parkiraliste parkiraliste = new org.foi.nwtis.globabic.ws.klijenti.Parkiraliste();

        parkiraliste.setId(p.getId());
        parkiraliste.setAdresa(p.getAdresa());
        parkiraliste.setKapacitet(p.getKapacitet());
        parkiraliste.setNaziv(p.getNaziv());
        parkiraliste.setStatus(p.getStatus());
        parkiraliste.setGeoloc(pretvoriLokaciju(p.getGeoloc()));

        return parkiraliste;
    }

    private org.foi.nwtis.globabic.ws.klijenti.Lokacija pretvoriLokaciju(Lokacija geoloc) {
        org.foi.nwtis.globabic.ws.klijenti.Lokacija lokacija = new org.foi.nwtis.globabic.ws.klijenti.Lokacija();
        lokacija.setLatitude(geoloc.getLatitude());
        lokacija.setLongitude(geoloc.getLongitude());
        return lokacija;
    }
}
