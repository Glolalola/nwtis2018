package org.foi.nwtis.globabic.rest.serveri;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Gloria Babić
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    /**
     *
     * @return
     */
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(org.foi.nwtis.globabic.rest.serveri.ParkiralisteREST.class);
    }

}
