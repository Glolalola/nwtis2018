/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.podaci;

/**
 *
 * @author Gloria Babić
 */
public enum StatusParkiralista {

    PASIVAN,
    AKTIVAN,
    BLOKIRAN,
    NEPOSTOJI;

    public String value() {
        return name();
    }

    public static StatusParkiralista fromValue(String v) {
        return valueOf(v);
    }

}
