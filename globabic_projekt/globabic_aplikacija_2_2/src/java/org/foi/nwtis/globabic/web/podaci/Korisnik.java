/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.podaci;

import java.util.Date;

/**
 *
 * @author Gloria Babić
 */
public class Korisnik {
    
    private int id;
    private String ki;
    private String ime;
    private String prezime;
    private String lozinka;
    private String email;
    private String vrsta;
    private Date datumKreiranja;
    private Date datumPromjene;

    public Korisnik() {
    }

    public Korisnik(int id, String korime, String ime, String prezime, String lozinka, String email, String vrsta, Date datumKreiranja, Date datumPromjene) {
        this.id = id;
        this.ki = korime;
        this.ime = ime;
        this.prezime = prezime;
        this.lozinka = lozinka;
        this.email = email;
        this.vrsta = vrsta;
        this.datumKreiranja = datumKreiranja;
        this.datumPromjene = datumPromjene;
    }

    public Korisnik(String korime, String ime, String prezime, String lozinka, String email) {
        this.ki = korime;
        this.ime = ime;
        this.prezime = prezime;
        this.lozinka = lozinka;
        this.email = email;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKi() {
        return ki;
    }

    public void setKi(String ki) {
        this.ki = ki;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }

    public Date getDatumKreiranja() {
        return datumKreiranja;
    }

    public void setDatumKreiranja(Date datumKreiranja) {
        this.datumKreiranja = datumKreiranja;
    }

    public Date getDatumPromjene() {
        return datumPromjene;
    }

    public void setDatumPromjene(Date datumPromjene) {
        this.datumPromjene = datumPromjene;
    }
    
    
    
}
