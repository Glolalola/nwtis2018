package org.foi.nwtis.globabic.web.zrna;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.foi.nwtis.globabic.ejb.eb.Dnevnik;
import org.foi.nwtis.globabic.ejb.sb.DnevnikFacade;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;

/**
 *
 * @author Gloria Babić
 */
@ManagedBean
@SessionScoped
public class Pogled_6 {

    @EJB
    private DnevnikFacade dnevnikFacade;

    private ServletContext sc = null;
    private Konfiguracija konf;

    private Integer id;
    private String korisnik;
    private String url;
    private String ipadresa;
    private Date vrijeme;
    private String trajanjeZahtjeva;
    private String adresaZahtjeva;
    private boolean tablica = false;
    private Date odVremena;
    private Date doVremena;
    private List<Dnevnik> podaciDnevnik = new ArrayList<>();
    private List<Dnevnik> podaciDnevnikZaPrikaz = new ArrayList<>();

    private int brojZapisaKojiSeMoguPrikazati;
    private int prvi = 0;
    private int korak;

    private boolean sljedeci = true, prethodni = true;

    /**
     * Creates a new instance of Pogled_6
     */
    public Pogled_6() {
    }

    @PostConstruct
    public void init() {

        sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        konf = (Konfiguracija) sc.getAttribute("konfiguracija");

        brojZapisaKojiSeMoguPrikazati = Integer.parseInt(konf.dajPostavku("brojPodatakaNastranici"));
        prvi = 0;
        prethodni = false;
        if (podaciDnevnikZaPrikaz.isEmpty()) {
            sljedeci = false;
        }
    }

    public String filtriraj() {
        prvi = 0;
        tablica = true;
        List<Dnevnik> podatci = dnevnikFacade.filtrirajDnevnike(korisnik, ipadresa, odVremena, doVremena, trajanjeZahtjeva, adresaZahtjeva);
        podaciDnevnik = podatci;

        if (podaciDnevnik.size() > brojZapisaKojiSeMoguPrikazati) {

            sljedeci = true;
        }
        if (podaciDnevnik.isEmpty()) {
            prethodni = false;
            tablica = false;
        }
        dohvatiZapiseZaPrikaz();
        return "";
    }

    private void dohvatiZapiseZaPrikaz() {
        podaciDnevnikZaPrikaz.clear();
        int kraj;
        if (prvi == 0) {
            prvi++;
            if (brojZapisaKojiSeMoguPrikazati > podaciDnevnik.size()) {
                kraj = podaciDnevnik.size();
            } else {
                kraj = brojZapisaKojiSeMoguPrikazati;
            }
            napuniListuZaPrikaz(kraj, 0);

        } else {
            if (brojZapisaKojiSeMoguPrikazati + korak > podaciDnevnik.size()) {
                kraj = podaciDnevnik.size();
                sljedeci = false;
            } else {
                kraj = brojZapisaKojiSeMoguPrikazati + korak;
            }
            napuniListuZaPrikaz(kraj, korak);

        }
    }

    private void napuniListuZaPrikaz(int brojZadnjeg, int brojPocetnog) {
        System.err.println("pocetak: " + brojPocetnog + "kraj: " + brojZadnjeg);
        for (int i = brojPocetnog; i < brojZadnjeg; i++) {
            podaciDnevnikZaPrikaz.add(podaciDnevnik.get(i));
        }
    }

    public String prethodni() {
        if (korak - brojZapisaKojiSeMoguPrikazati > 0) {
            korak -= brojZapisaKojiSeMoguPrikazati;
        } else {
            korak = 0;
            prethodni = false;
        }
        sljedeci = true;
        dohvatiZapiseZaPrikaz();
        return "";
    }

    public String sljedeci() {
        prethodni = true;

        korak += brojZapisaKojiSeMoguPrikazati;

        dohvatiZapiseZaPrikaz();
        return "";
    }

    public DnevnikFacade getDnevnikFacade() {
        return dnevnikFacade;
    }

    public void setDnevnikFacade(DnevnikFacade dnevnikFacade) {
        this.dnevnikFacade = dnevnikFacade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(String korisnik) {
        this.korisnik = korisnik;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIpadresa() {
        return ipadresa;
    }

    public void setIpadresa(String ipadresa) {
        this.ipadresa = ipadresa;
    }

    public Date getVrijeme() {
        return vrijeme;
    }

    public void setVrijeme(Date vrijeme) {
        this.vrijeme = vrijeme;
    }

    public String getTrajanjeZahtjeva() {
        return trajanjeZahtjeva;
    }

    public void setTrajanjeZahtjeva(String trajanjeZahtjeva) {
        this.trajanjeZahtjeva = trajanjeZahtjeva;
    }

    public String getAdresaZahtjeva() {
        return adresaZahtjeva;
    }

    public void setAdresaZahtjeva(String adresaZahtjeva) {
        this.adresaZahtjeva = adresaZahtjeva;
    }

    public boolean isTablica() {
        return tablica;
    }

    public void setTablica(boolean tablica) {
        this.tablica = tablica;
    }

    public Date getOdVremena() {
        return odVremena;
    }

    public void setOdVremena(Date odVremena) {
        this.odVremena = odVremena;
    }

    public Date getDoVremena() {
        return doVremena;
    }

    public void setDoVremena(Date doVremena) {
        this.doVremena = doVremena;
    }

    public List<Dnevnik> getPodaciDnevnik() {
        return podaciDnevnik;
    }

    public void setPodaciDnevnik(List<Dnevnik> podaciDnevnik) {
        this.podaciDnevnik = podaciDnevnik;
    }

    public List<Dnevnik> getPodaciDnevnikZaPrikaz() {
        return podaciDnevnikZaPrikaz;
    }

    public void setPodaciDnevnikZaPrikaz(List<Dnevnik> podaciDnevnikZaPrikaz) {
        this.podaciDnevnikZaPrikaz = podaciDnevnikZaPrikaz;
    }

    public boolean isSljedeci() {
        return sljedeci;
    }

    public void setSljedeci(boolean sljedeci) {
        this.sljedeci = sljedeci;
    }

    public boolean isPrethodni() {
        return prethodni;
    }

    public void setPrethodni(boolean prethodni) {
        this.prethodni = prethodni;
    }

}
