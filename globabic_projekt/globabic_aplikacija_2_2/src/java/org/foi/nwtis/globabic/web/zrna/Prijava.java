/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.zrna;

import com.google.gson.JsonObject;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.foi.nwtis.globabic.rest.klijenti.KorisniciREST;

/**
 *
 * @author Gloria Babić
 */
@ManagedBean
@SessionScoped
public class Prijava implements Serializable {

    private String korisnickoIme;
    private String korisnickaLozinka;
    private String poruka = "";

    /**
     * Creates a new instance of Prijava
     */
    public Prijava() {
    }

    public String prijaviSe() {

        if (korisnickaLozinka.isEmpty() || korisnickoIme.isEmpty()) {
            poruka = "Niste popunili sva polja!";
        }
        JsonObject jsonObject = null;
        KorisniciREST korisniciREST = new KorisniciREST();
        String odgovor = korisniciREST.postJson(jsonObject, String.class, "NE", korisnickoIme, korisnickaLozinka);
        System.err.println("OGOVOR SA REST KORISNICI: " + odgovor);
        if (odgovor.contains("OK")) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
            session.setAttribute("korime", korisnickoIme);
            session.setAttribute("lozinka", korisnickaLozinka);

            Lokalizacija.prijavljen = true;
            return "ok";
        } else {
            poruka = "Vaše korisničko ime ili lozinka su pogrešni.";
            return "no";
        }

    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getKorisnickaLozinka() {
        return korisnickaLozinka;
    }

    public void setKorisnickaLozinka(String korisnickaLozinka) {
        this.korisnickaLozinka = korisnickaLozinka;
    }

    public String getPoruka() {
        return poruka;
    }

    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }

}
