/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.ws.klijenti;

/**
 *
 * @author Gloria Babić
 */
public class MeteoWSKlijent {

    public static java.util.List<org.foi.nwtis.globabic.ws.klijenti.MeteoPodaci> dajNMeteoPodatka(java.lang.String korime, java.lang.String lozinka, int id, int n) {
        org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS_Service service = new org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS_Service();
        org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS port = service.getGeoMeteoWSPort();
        return port.dajNMeteoPodatka(korime, lozinka, id, n);
    }

    public static java.util.List<org.foi.nwtis.globabic.ws.klijenti.MeteoPodaci> dajSveMeteoPodatke(java.lang.String korime, java.lang.String lozinka, int idParkiralista, long odVremena, long doVremena) {
        org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS_Service service = new org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS_Service();
        org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS port = service.getGeoMeteoWSPort();
        return port.dajSveMeteoPodatke(korime, lozinka, idParkiralista, odVremena, doVremena);
    }

    public static MeteoPodaci dajVazeceMeteoPodatke(java.lang.String korime, java.lang.String lozinka, int id) {
        org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS_Service service = new org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS_Service();
        org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS port = service.getGeoMeteoWSPort();
        return port.dajVazeceMeteoPodatke(korime, lozinka, id);
    }

    public static MeteoPodaci dajZadnjeMeteoPodatke(java.lang.String korime, java.lang.String lozinka, int id) {
        org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS_Service service = new org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS_Service();
        org.foi.nwtis.globabic.ws.klijenti.GeoMeteoWS port = service.getGeoMeteoWSPort();
        return port.dajZadnjeMeteoPodatke(korime, lozinka, id);
    }
    
    
    
}
