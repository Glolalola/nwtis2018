package org.foi.nwtis.globabic.web.slusaci;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.konfiguracije.KonfiguracijaApstraktna;
import org.foi.nwtis.globabic.konfiguracije.NeispravnaKonfiguracija;
import org.foi.nwtis.globabic.konfiguracije.NemaKonfiguracije;

/**
 * Inicijalizira kontekst i pokreće upravljačku dretvu
 *
 * @author Gloria Babić
 */
public class SlusacAplikacije implements ServletContextListener {

    private static ServletContext servletContext;
    public static Konfiguracija konfiguracija = null;

    public static ServletContext getServletContext() {
        return servletContext;
    }  
    

    /**
     * Inicijalizira kontekst i pokreće pozadinsku dretvu
     *
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        servletContext = sce.getServletContext();
        String datoteka = servletContext.getInitParameter("konfiguracija");
        String putanja = servletContext.getRealPath("/WEB-INF") + java.io.File.separator;
        String puniNazivDatoteke = putanja + datoteka;

        

        try {
            konfiguracija = KonfiguracijaApstraktna.preuzmiKonfiguraciju(puniNazivDatoteke);
            servletContext.setAttribute("konfiguracija", konfiguracija);
            

        } catch (NemaKonfiguracije | NeispravnaKonfiguracija ex) {
            Logger.getLogger(SlusacAplikacije.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     *
     * @param sce
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
       
    }
}
