package org.foi.nwtis.globabic.web.zrna;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.foi.nwtis.globabic.rest.klijenti.ParkiralistaREST;
import org.foi.nwtis.globabic.rest.klijenti.ParkiralistaREST_ID;
import org.foi.nwtis.globabic.rest.klijenti.ParkiralistaREST_Vozila;
import org.foi.nwtis.globabic.web.podaci.Izbornik;
import org.foi.nwtis.globabic.web.podaci.Lokacija;
import org.foi.nwtis.globabic.web.podaci.Parkiraliste;
import org.foi.nwtis.globabic.web.podaci.StatusParkiralista;
import org.foi.nwtis.globabic.web.podaci.Vozilo;
import org.foi.nwtis.globabic.ws.klijenti.MeteoWSKlijent;

/**
 *
 * @author Gloria Babić
 */
@ManagedBean
@SessionScoped
public class Pogled_3 implements Serializable {

    private String adresa;
    private String naziv;
    private String kapacitet;
    private String odabraniParking;
    private String statusOdabranogParkiralista;
    private String poruka;
    private String korime;
    private String lozinka;
    private List<Izbornik> vlastitiPopisParkinga = new ArrayList<>();
    private List<Vozilo> popisVozila = new ArrayList<>();
    private org.foi.nwtis.globabic.ws.klijenti.MeteoPodaci meteoPodaci;
    private List<Parkiraliste> parkiralista = new ArrayList<>();
    Comparator<Izbornik> comparatorIzbornika = Comparator.comparing(
            Izbornik::getLabela,
            String.CASE_INSENSITIVE_ORDER);

    @PostConstruct
    public void init() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        korime = session.getAttribute("korime").toString();
        lozinka = session.getAttribute("lozinka").toString();
        dohvatiParkiralista();
        odabraniParking = vlastitiPopisParkinga.get(0).getVrijednost();
    }

    public void dohvatiParkiralista() {
        
        vlastitiPopisParkinga = new ArrayList<>();
        ParkiralistaREST prest = new ParkiralistaREST();
        String json = prest.getJson(String.class, korime, lozinka);
        parkiralista = pripremiJsonZaNiz(json);

        for (Parkiraliste p : parkiralista) {
            Izbornik i = new Izbornik(p.getNaziv(),
                    Integer.toString(p.getId()));
            vlastitiPopisParkinga.add(i);
        }
        vlastitiPopisParkinga.sort(comparatorIzbornika);
    }

    private List<Parkiraliste> pripremiJsonZaNiz(String json) {
        List<Parkiraliste> listaParkiralista = new ArrayList<>();
        Gson gson = new Gson();

        JsonElement jsonElement = new JsonParser().parse(json);
        com.google.gson.JsonObject jsonObject = jsonElement.getAsJsonObject();
        com.google.gson.JsonArray jsonArray = jsonObject.getAsJsonArray("odgovor");

        for (JsonElement jsonParkiraliste : jsonArray) {
            Parkiraliste p = gson.fromJson(jsonParkiraliste, Parkiraliste.class);
            Lokacija l = gson.fromJson(jsonParkiraliste, Lokacija.class);
            p.setGeoloc(l);
            listaParkiralista.add(p);
        }
        return listaParkiralista;

    }

    public String dodajParkiraliste() {
        if (naziv.isEmpty() || adresa.isEmpty() || kapacitet.isEmpty()) {
            poruka = "Sva polja moraju biti popunjena!";
            return "";
        }
        Parkiraliste p = new Parkiraliste(adresa, Integer.parseInt(kapacitet), naziv);
        String parkiraliste = p.toStringDodaj();
        ParkiralistaREST prest = new ParkiralistaREST();
        String odgovorKlijenta = prest.postJson(parkiraliste, String.class, korime, lozinka);
        poruka = odgovorKlijenta;
        dohvatiParkiralista();
        vlastitiPopisParkinga.sort(comparatorIzbornika);
        return "";
    }

    public String dohvatiStatusParkiralista() {
        Parkiraliste parkiraliste = odabranoParkiraliste();
        statusOdabranogParkiralista = parkiraliste.getStatus().value().toLowerCase();
        poruka = "Dohvaćen status: " + statusOdabranogParkiralista + " za: " + parkiraliste.getNaziv();
        return "";
    }

    private Parkiraliste odabranoParkiraliste() {
        int idOdabraniParking = Integer.parseInt(odabraniParking);
        Parkiraliste parkiraliste = new Parkiraliste();
        for (Parkiraliste p : parkiralista) {
            if (p.getId() == idOdabraniParking) {
                parkiraliste = p;
            }
        }
        return parkiraliste;
    }

    public String obrisiParkiraliste() {
        ParkiralistaREST_ID prestid = new ParkiralistaREST_ID(odabraniParking);
        String odgovorKlijenta = prestid.deleteJson(String.class, korime, lozinka);

        dohvatiParkiralista();
        vlastitiPopisParkinga.sort(comparatorIzbornika);
        poruka = "Odgovor klijenta za: " + odabranoParkiraliste().getNaziv() + " : " + odgovorKlijenta;
        odabraniParking = vlastitiPopisParkinga.get(0).getVrijednost();        
        return "";
    }

    public String aktivirajParkiraliste() {
        Parkiraliste parkiraliste = new Parkiraliste();
        parkiraliste = odabranoParkiraliste();
        parkiraliste.setStatus(StatusParkiralista.AKTIVAN);
        String parkiralisteZaKlijenta = parkiraliste.toStringAzuriraj();

        ParkiralistaREST_ID prestid = new ParkiralistaREST_ID(odabraniParking);
        String odgovorKlijenta = prestid.putJson(parkiralisteZaKlijenta, String.class, korime, lozinka);

        dohvatiParkiralista();
        vlastitiPopisParkinga.sort(comparatorIzbornika);
        poruka = "Odgovor klijenta za: " + parkiraliste.getNaziv() + " : " + odgovorKlijenta;
        return "";
    }

    public String blokirajParkiraliste() {
        Parkiraliste parkiraliste = new Parkiraliste();
        parkiraliste = odabranoParkiraliste();
        parkiraliste.setStatus(StatusParkiralista.BLOKIRAN);
        String parkiralisteZaKlijenta = parkiraliste.toStringAzuriraj();

        ParkiralistaREST_ID prestid = new ParkiralistaREST_ID(odabraniParking);
        String odgovorKlijenta = prestid.putJson(parkiralisteZaKlijenta, String.class, korime, lozinka);

        dohvatiParkiralista();
        vlastitiPopisParkinga.sort(comparatorIzbornika);
        poruka = "Odgovor klijenta za: " + parkiraliste.getNaziv() + " : " + odgovorKlijenta;
        return "";
    }

    public String dohvatiVozilaParkiralista() {

        ParkiralistaREST_Vozila prestv = new ParkiralistaREST_Vozila(odabraniParking);
        String odgovorKlijenta = prestv.getJsonCars(String.class, korime, lozinka);

        popisVozila = pripremiJsonZaNizVozila(odgovorKlijenta);

        poruka = "Vozila dohvaćena za: " + odabranoParkiraliste().getNaziv();

        return "";
    }

    private List<Vozilo> pripremiJsonZaNizVozila(String json) {
        List<Vozilo> listaVozila = new ArrayList<>();
        Gson gson = new Gson();

        JsonElement jsonElement = new JsonParser().parse(json);
        com.google.gson.JsonObject jsonObject = jsonElement.getAsJsonObject();
        com.google.gson.JsonArray jsonArray = jsonObject.getAsJsonArray("odgovor");

        for (JsonElement jsonVozilo : jsonArray) {
            Vozilo v = gson.fromJson(jsonVozilo, Vozilo.class);
            listaVozila.add(v);
        }
        return listaVozila;

    }

    public String dohvatiZadnjeMeteo() {

        int idOdabraniParking = Integer.parseInt(odabraniParking);
        meteoPodaci = MeteoWSKlijent.dajZadnjeMeteoPodatke(korime, lozinka, idOdabraniParking);
        if (meteoPodaci == null) {
            poruka = "Nije moguće zadnje vazece meteopodatke.";
            return "";
        }

        poruka = "Dohvaceni su zadnji meteopodaci za: " + odabranoParkiraliste().getNaziv();
        return "";
    }

    public String dohvatiVazeceMeteo() {
        int idOdabraniParking = Integer.parseInt(odabraniParking);
        meteoPodaci = MeteoWSKlijent.dajVazeceMeteoPodatke(korime, lozinka, idOdabraniParking);
        if (meteoPodaci == null) {
            poruka = "Nije moguće dohvatiti važeće meteopodatke za: " + odabranoParkiraliste().getNaziv();
            return "";
        }

        poruka = "Dohvaceni su važeći meteopodaci za: " + odabranoParkiraliste().getNaziv();
        return "";
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getKapacitet() {
        return kapacitet;
    }

    public void setKapacitet(String kapacitet) {
        this.kapacitet = kapacitet;
    }

    public String getOdabraniParking() {
        return odabraniParking;
    }

    public void setOdabraniParking(String odabraniParking) {
        this.odabraniParking = odabraniParking;
    }

    public List<Izbornik> getVlastitiPopisParkinga() {
        return vlastitiPopisParkinga;
    }

    public void setVlastitiPopisParkinga(List<Izbornik> vlastitiPopisParkinga) {
        this.vlastitiPopisParkinga = vlastitiPopisParkinga;
    }

    public String getPoruka() {
        return poruka;
    }

    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }

    public void setPopisVozila(List<Vozilo> popisVozila) {
        this.popisVozila = popisVozila;
    }

    public List<Vozilo> getPopisVozila() {
        return popisVozila;
    }

    public org.foi.nwtis.globabic.ws.klijenti.MeteoPodaci getMeteoPodaci() {
        return meteoPodaci;
    }

    public void setMeteoPodaci(org.foi.nwtis.globabic.ws.klijenti.MeteoPodaci meteoPodaci) {
        this.meteoPodaci = meteoPodaci;
    }

}
