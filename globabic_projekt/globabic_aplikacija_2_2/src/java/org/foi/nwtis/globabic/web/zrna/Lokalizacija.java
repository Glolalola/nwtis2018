/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.zrna;

import java.io.Serializable;
import java.util.Locale;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.foi.nwtis.globabic.ejb.sb.DnevnikFacade;

/**
 *
 * @author Gloria Babić
 */
@Named(value = "lokalizacija")
@SessionScoped
public class Lokalizacija implements Serializable {

    @EJB
    private DnevnikFacade dnevnikFacade;

    private String odabraniJezik = "hr";

    public static boolean prijavljen = false;

    /**
     * Creates a new instance of Lokalizacija
     */
    public Lokalizacija() {

    }

    public String getOdabraniJezik() {
        return odabraniJezik;
    }

    public Object odaberiJezik(String jezik) {

        Locale locale = new Locale(jezik);
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
        odabraniJezik = jezik;

        return "";
    }

    public String odjaviSe() {
        prijavljen = false;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        session.invalidate();

        return "prijava.xhtml";
    }

    public boolean isPrijavljen() {
        return prijavljen;
    }

    public void setPrijavljen(boolean prijavljen) {
        this.prijavljen = prijavljen;
    }

}
