/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.zrna;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.foi.nwtis.globabic.ejb.eb.Dnevnik;
import org.foi.nwtis.globabic.ejb.sb.DnevnikFacade;

/**
 * Pruža pristup Dnevnik fasadi ostatku aplikacije.
 */
@Named("dnevnikBean")
@ApplicationScoped
public class DohvatDnevnikfascade {

    @EJB
    private DnevnikFacade dnevnikFacade;
    
    public void zapisiUDnevnik(Dnevnik dnevnik) {        
        dnevnikFacade.create(dnevnik);
    }
    
}
