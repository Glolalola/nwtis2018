/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.podaci;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 *
 * @author mruzman
 */
public class Parkiraliste {

    protected String adresa;
    protected Lokacija geoloc;
    protected int id;
    protected int kapacitet;
    protected String naziv;
    protected StatusParkiralista status;

    public Parkiraliste() {
    }

    public Parkiraliste(String adresa, Lokacija geoloc, int id, int kapacitet, String naziv, StatusParkiralista status) {
        this.adresa = adresa;
        this.geoloc = geoloc;
        this.id = id;
        this.kapacitet = kapacitet;
        this.naziv = naziv;
        this.status = status;
    }

    public Parkiraliste(String adresa, Lokacija geoloc, int kapacitet, String naziv, StatusParkiralista status) {
        this.adresa = adresa;
        this.geoloc = geoloc;
        this.kapacitet = kapacitet;
        this.naziv = naziv;
        this.status = status;
    }

    public Parkiraliste(String adresa, int kapacitet, String naziv) {
        this.adresa = adresa;
        this.kapacitet = kapacitet;
        this.naziv = naziv;
    }
    
    

    public Lokacija getGeoloc() {
        return geoloc;
    }

    public void setGeoloc(Lokacija geoloc) {
        this.geoloc = geoloc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public int getKapacitet() {
        return kapacitet;
    }

    public void setKapacitet(int kapacitet) {
        this.kapacitet = kapacitet;
    }

    public StatusParkiralista getStatus() {
        return status;
    }

    public void setStatus(StatusParkiralista status) {
        this.status = status;
    }

    public  String toStringDodaj() {
        Gson gson = new Gson();
        com.google.gson.JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("naziv", naziv);
        jsonObject.addProperty("adresa", adresa);
        jsonObject.addProperty("kapacitet", kapacitet);
        return jsonObject.toString();

    }

    public String toStringAzuriraj() {
        Gson gson = new Gson();
        com.google.gson.JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("naziv", naziv);
        jsonObject.addProperty("adresa", adresa);
        jsonObject.addProperty("kapacitet", kapacitet);
        jsonObject.addProperty("status", status.value());
        return jsonObject.toString();

    }

}
