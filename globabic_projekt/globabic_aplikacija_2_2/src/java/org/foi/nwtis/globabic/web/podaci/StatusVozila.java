
package org.foi.nwtis.globabic.web.podaci;


public enum StatusVozila {

    ULAZ,
    IZLAZ;

    public String value() {
        return name();
    }

    public static StatusVozila fromValue(String v) {
        return valueOf(v);
    }

}
