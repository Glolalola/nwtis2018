/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.filteri;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Date;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.foi.nwtis.globabic.ejb.eb.Dnevnik;
import org.foi.nwtis.globabic.web.zrna.DohvatDnevnikfascade;

/**
 *
 * @author Gloria Babić
 */
@WebFilter(filterName = "AplikativniFilterDnevnik", urlPatterns = {"/*"})
public class AplikativniFilterDnevnik implements Filter {

    @Inject
    private DohvatDnevnikfascade dohvatDnevnikfascade;

    private static final boolean debug = true;
    private FilterConfig filterConfig = null;

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        Dnevnik dnevnik = new Dnevnik();
        dnevnik.setVrijeme(new Date(System.currentTimeMillis()));
        dnevnik.setUrl(((HttpServletRequest) request).getRequestURL().toString());
        dnevnik.setIpadresa(InetAddress.getLocalHost().getHostAddress());
        HttpServletRequest hsr = (HttpServletRequest) request;
        HttpServletResponse hsr1 = (HttpServletResponse) response;
        HttpSession session = hsr.getSession();

        if (session != null) {
            String korime = (String) session.getAttribute("korime");
            if (korime != null) {
                dnevnik.setKorisnik(korime);
            } else {
                dnevnik.setKorisnik("anonimni_korisnik");
            }

        } else {
            dnevnik.setKorisnik("anonimni_korisnik");
        }

        if (debug) {
            log("DNEVNIK: " + dnevnik.getVrijeme() + dnevnik.getUrl() + "adresa " + dnevnik.getIpadresa() + "korisnik " + dnevnik.getKorisnik() + " status " + dnevnik.getStatus());
        }

        long start = System.currentTimeMillis();
        chain.doFilter(hsr, hsr1);
        long end = System.currentTimeMillis();

        dnevnik.setTrajanje((int) (end - start));
        dnevnik.setStatus(hsr1.getStatus());

        dohvatDnevnikfascade.zapisiUDnevnik(dnevnik);
        if (debug) {
            log("Jel se zapisalo?");
        }

    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }

    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {

        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("AplikativniFilterDnevnik:Initializing filter");
            }
        }

    }

}
