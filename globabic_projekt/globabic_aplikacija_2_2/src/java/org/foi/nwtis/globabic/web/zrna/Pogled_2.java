package org.foi.nwtis.globabic.web.zrna;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.web.slusaci.SlusacAplikacije;

/**
 *
 * @author Gloria Babić
 */
@ManagedBean
@SessionScoped
public class Pogled_2 implements Serializable {

    private String stanjeServera;
    private String stanjeGrupe;

    String korisnik;
    String lozinka;

    private Konfiguracija konfiguracija = SlusacAplikacije.konfiguracija;

    @PostConstruct
    public void init() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        korisnik = session.getAttribute("korime").toString();
        lozinka = session.getAttribute("lozinka").toString();

        this.stanjeGrupe = dohvatiStanjeGrupe();
        this.stanjeServera = dohvatiStanjeServera();

    }

    public String dohvatiStanjeServera() {
        String stanje = "";
        try {
            stanje = "Stanje servera:" + posaljiKomanduServera("STANJE");
        } catch (Exception e) {
            stanje = "Server je ugašen.";
        }
        return stanje;
    }

    public String dohvatiStanjeGrupe() {
        String stanje = "";
        try {
            stanje = "Stanje grupe:" + posaljiKomanduGrupe("STANJE");
        } catch (Exception e) {
            stanje = "Nemoguće dohvatiti stanje grupe.";
        }
        return stanje;

    }

    public String pauziraj() {
        stanjeServera = posaljiKomanduServera("PAUZA") + dohvatiStanjeServera();
        return "";
    }

    public String pokreni() {
        stanjeServera = posaljiKomanduServera("KRENI") + dohvatiStanjeServera();
        return "";
    }

    public String pasiviziraj() {
        stanjeServera = posaljiKomanduServera("PASIVNO") + dohvatiStanjeServera();
        return "";
    }

    public String aktiviraj() {
        stanjeServera = posaljiKomanduServera("AKTIVNO") + dohvatiStanjeServera();
        return "";
    }

    public String zaustavi() {
        stanjeServera = posaljiKomanduServera("STANI") + "Server je zaustavljen i više" + System.lineSeparator() + " se ne mogu slati komande.";
        return "";
    }

    public String deregistrirajGrupu() {
        stanjeGrupe = posaljiKomanduGrupe("PREKID");// + dohvatiStanjeGrupe();
        return "";
    }

    public String registrirajGrupu() {
        stanjeGrupe = posaljiKomanduGrupe("DODAJ");// + dohvatiStanjeGrupe();

        return "";
    }

    public String blokirajGrupu() {
        stanjeGrupe = posaljiKomanduGrupe("PAUZA");// + dohvatiStanjeGrupe();

        return "";
    }

    public String aktivirajGrupu() {
        stanjeGrupe = posaljiKomanduGrupe("KRENI");// + dohvatiStanjeGrupe();

        return "";
    }

    private String posaljiKomanduServera(String imeKomande) {

        String komanda = "KORISNIK " + korisnik + "; LOZINKA " + lozinka + "; " + imeKomande + ";";
        String odgovor = posalji(komanda);

        return odgovor;
    }

    private String posaljiKomanduGrupe(String imeKomande) {

        String komanda = "KORISNIK " + korisnik + "; LOZINKA " + lozinka + "; GRUPA " + imeKomande + ";";
        String odgovor = posalji(komanda);

        return odgovor;
    }

    public String posalji(String komanda) {
        String adresa = konfiguracija.dajPostavku("adresa");
        int port = Integer.parseInt(konfiguracija.dajPostavku("port"));
        String odgovor = "";

        try {
            Socket socket = new Socket(adresa, port);
            InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();
            StringBuffer buffer = new StringBuffer();
            os.write(komanda.getBytes());
            os.flush();
            socket.shutdownOutput();
            while (true) {
                int znak = is.read();
                if (znak == -1) {
                    break;
                }
                buffer.append((char) znak);
            }
            is.close();
            odgovor = buffer.toString();
        } catch (SocketTimeoutException ex) {
            odgovor = ex.getMessage();
        } catch (IOException ex) {
            Logger.getLogger(Pogled_2.class.getName()).log(Level.SEVERE, null, ex);
        }

        return odgovor;
    }

    public String getStanjeServera() {
        return stanjeServera;
    }

    public void setStanjeServera(String stanjeServera) {
        this.stanjeServera = stanjeServera;
    }

    public String getStanjeGrupe() {
        return stanjeGrupe;
    }

    public void setStanjeGrupe(String stanjeGrupe) {
        this.stanjeGrupe = stanjeGrupe;
    }

}
