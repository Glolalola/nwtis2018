/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.zrna;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.foi.nwtis.globabic.rest.klijenti.KorisniciREST;

/**
 *
 * @author Gloria Babić
 */
@ManagedBean
@SessionScoped
public class Registracija implements Serializable {

    private String ime;
    private String prezime;
    private String porukaDodaj = "";

    private String korisnickoIme;
    private String korisnickaLozinka;
    private String ponovljenaLozinka;

    /**
     * Creates a new instance of Prijava
     */
    public Registracija() {
    }

    public String registriraj() {
        if (provjeraParametara()) {
            return "";
        }
        String podaci = oblikujPodatke();
        KorisniciREST krest = new KorisniciREST();
        String odgovor = krest.postJson(podaci, String.class, "DA", korisnickoIme, korisnickaLozinka);
        ispisPoruke(odgovor);

        return "";
    }

    private void ispisPoruke(String odgovor) {
        System.err.println("ODGOVOR PRI REGISTRACIJI: " + odgovor);
        if (odgovor.contains("OK")) {
            porukaDodaj = "Uspjesno dodan korisnik.";
        } else if (odgovor.contains("ERR 10;")) {
            porukaDodaj = "Korisničko ime već postoji.";
        } else {
            porukaDodaj = "Greška pri registraciji.";
        }
    }

    private boolean provjeraParametara() {
        if (ime.isEmpty() || prezime.isEmpty() || korisnickoIme.isEmpty() || korisnickaLozinka.isEmpty() || ponovljenaLozinka.isEmpty()) {
            porukaDodaj = "Sva polja moraju biti popunjena!";
            return true;
        }
        if (!ime.matches("[a-zA-Z]+") || !prezime.matches("[a-zA-Z]+")) {
            porukaDodaj = "Ne smiejte unositi hrvatske znakove!";
            return true;
        }

        if (!korisnickaLozinka.equals(ponovljenaLozinka)) {
            porukaDodaj = "Krivo ste upisali lozinku!";
            return true;
        }
        if (korisnickoIme.length() > 9) {
            porukaDodaj = "Korisnicko ime smije biti do 9 znakova duljine!";
            return true;
        }
        return false;
    }

    private String oblikujPodatke() {
        Gson gson = new Gson();
        com.google.gson.JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ki", korisnickoIme);
        jsonObject.addProperty("lozinka", korisnickaLozinka);
        jsonObject.addProperty("ime", ime);
        jsonObject.addProperty("prezime", prezime);
        return jsonObject.toString();
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getPorukaDodaj() {
        return porukaDodaj;
    }

    public void setPorukaDodaj(String porukaDodaj) {
        this.porukaDodaj = porukaDodaj;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getKorisnickaLozinka() {
        return korisnickaLozinka;
    }

    public void setKorisnickaLozinka(String korisnickaLozinka) {
        this.korisnickaLozinka = korisnickaLozinka;
    }

    public String getPonovljenaLozinka() {
        return ponovljenaLozinka;
    }

    public void setPonovljenaLozinka(String ponovljenaLozinka) {
        this.ponovljenaLozinka = ponovljenaLozinka;
    }

}
