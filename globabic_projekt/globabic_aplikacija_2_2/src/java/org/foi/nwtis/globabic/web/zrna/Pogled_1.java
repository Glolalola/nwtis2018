/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.zrna;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.rest.klijenti.KorisniciREST;
import org.foi.nwtis.globabic.rest.klijenti.KorisniciREST_Korime;
import org.foi.nwtis.globabic.web.podaci.Korisnik;

/**
 *
 * @author Gloria Babić
 */
@ManagedBean
@SessionScoped
public class Pogled_1 implements Serializable {

    private ServletContext sc = null;
    private Konfiguracija konf;

    private String imeKorisnika;
    private String prezimeKorisnika;
    private String porukaAzuriraj = "";

    private String prijavljeniKorisnik;
    private String lozinkaPrijavljenog;

    private List<Korisnik> korisnici = new ArrayList<>();
    private List<Korisnik> korisniciZaPrikaz = new ArrayList<>();

    private int brojKorisnikaKojiSeMoguPrikazati;
    private int brojPocetnogKorisnika;
    private int brojZadnjegKorisnika;
    private int prvi = 0;
    private int korak;

    private boolean nemaKorisnika = true;
    private boolean sljedeci = true, prethodni = true;

    /**
     * Creates a new instance of Pogled_1
     */
    public Pogled_1() {

    }

    @PostConstruct
    public void init() {

        sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        konf = (Konfiguracija) sc.getAttribute("konfiguracija");

        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        prijavljeniKorisnik = session.getAttribute("korime").toString();
        lozinkaPrijavljenog = session.getAttribute("lozinka").toString();
        brojKorisnikaKojiSeMoguPrikazati = Integer.parseInt(konf.dajPostavku("brojPodatakaNastranici"));
        System.err.println("prijavljeniKorisnik " + prijavljeniKorisnik);
        prvi = 0;
        prethodni = false;
        dohvatiSveKorisnike();
        dohvatiKorisnikeZaPrikaz();
        dohvatiPodatkePrijavljenogKorisnika();

    }

    private void dohvatiKorisnikeZaPrikaz() {
        korisniciZaPrikaz.clear();
        int kraj;
        if (prvi == 0) {
            prvi++;
            if (brojKorisnikaKojiSeMoguPrikazati > korisnici.size()) {
                kraj = korisnici.size();
            } else {
                kraj = brojKorisnikaKojiSeMoguPrikazati;
            }
            napuniListuZaPrikaz(kraj, 0);

        } else {
            if (brojKorisnikaKojiSeMoguPrikazati + korak >= korisnici.size()) {
                kraj = korisnici.size();
                sljedeci = false;
            } else {
                kraj = brojKorisnikaKojiSeMoguPrikazati + korak;
            }
            napuniListuZaPrikaz(kraj, korak);

        }
    }

    private void dohvatiSveKorisnike() {
        KorisniciREST krest = new KorisniciREST();
        String odgovor = krest.getJson(String.class, prijavljeniKorisnik, lozinkaPrijavljenog);
        korisnici = pripremiJsonZaNiz(odgovor);
        if (korisnici.size() < brojKorisnikaKojiSeMoguPrikazati) {
            sljedeci = false;
        }
    }

    private List<Korisnik> pripremiJsonZaNiz(String json) {
        System.err.println("ODGOVOR; " + json);
        List<Korisnik> listaKorisnika = new ArrayList<>();
        Gson gson = new Gson();

        JsonElement jsonElement = new JsonParser().parse(json);
        com.google.gson.JsonObject jsonObject = jsonElement.getAsJsonObject();
        com.google.gson.JsonArray jsonArray = jsonObject.getAsJsonArray("odgovor");

        for (JsonElement jsonParkiraliste : jsonArray) {
            Korisnik k = gson.fromJson(jsonParkiraliste, Korisnik.class);
            listaKorisnika.add(k);
        }
        return listaKorisnika;

    }

    public String azuriraj() {

        if (imeKorisnika.isEmpty() || prezimeKorisnika.isEmpty()) {
            porukaAzuriraj = "Sva polja moraju biti popunjena!";
            return "";
        }

        String podaci = oblikujPodatke();
        KorisniciREST_Korime krestk = new KorisniciREST_Korime(prijavljeniKorisnik);
        String odgovor = krestk.putJson(podaci, String.class, prijavljeniKorisnik, lozinkaPrijavljenog);
        porukaAzuriraj = odgovor;
        dohvatiSveKorisnike();

        return "";
    }

//    private void dohvatiPodatkeIzSesije() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    public String prethodniKorisnici() {
        if (korak - brojKorisnikaKojiSeMoguPrikazati > 0) {
            korak -= brojKorisnikaKojiSeMoguPrikazati;
        } else {
            korak = 0;
            prethodni = false;
        }
        sljedeci = true;
        dohvatiKorisnikeZaPrikaz();
        return "";
    }

    public String sljedeciKorisnici() {
        prethodni = true;
        System.err.println("KORAK; " + korak);
        korak += brojKorisnikaKojiSeMoguPrikazati;
        System.err.println("KORAK2; " + korak);
        dohvatiKorisnikeZaPrikaz();
        return "";
    }

    public ServletContext getSc() {
        return sc;
    }

    public void setSc(ServletContext sc) {
        this.sc = sc;
    }

    public Konfiguracija getKonf() {
        return konf;
    }

    public void setKonf(Konfiguracija konf) {
        this.konf = konf;
    }

    public String getPorukaAzuriraj() {
        return porukaAzuriraj;
    }

    public void setPorukaAzuriraj(String porukaAzuriraj) {
        this.porukaAzuriraj = porukaAzuriraj;
    }

    public String getImeKorisnika() {
        return imeKorisnika;
    }

    public void setImeKorisnika(String imeKorisnika) {
        this.imeKorisnika = imeKorisnika;
    }

    public String getPrezimeKorisnika() {
        return prezimeKorisnika;
    }

    public void setPrezimeKorisnika(String prezimeKorisnika) {
        this.prezimeKorisnika = prezimeKorisnika;
    }

    public List<Korisnik> getKorisnici() {
        return korisnici;
    }

    public void setKorisnici(List<Korisnik> korisnici) {
        this.korisnici = korisnici;
    }

    public int getBrojKorisnikaKojiSeMoguPrikazati() {
        return brojKorisnikaKojiSeMoguPrikazati;
    }

    public void setBrojKorisnikaKojiSeMoguPrikazati(int brojKorisnikaKojiSeMoguPrikazati) {
        this.brojKorisnikaKojiSeMoguPrikazati = brojKorisnikaKojiSeMoguPrikazati;
    }

    public int getBrojPocetnogKorisnika() {
        return brojPocetnogKorisnika;
    }

    public void setBrojPocetnogKorisnika(int brojPocetnogKorisnika) {
        this.brojPocetnogKorisnika = brojPocetnogKorisnika;
    }

    public int getBrojZadnjegKorisnika() {
        return brojZadnjegKorisnika;
    }

    public void setBrojZadnjegKorisnika(int brojZadnjegKorisnika) {
        this.brojZadnjegKorisnika = brojZadnjegKorisnika;
    }

    public int getPrvi() {
        return prvi;
    }

    public void setPrvi(int prvi) {
        this.prvi = prvi;
    }

    public int getKorak() {
        return korak;
    }

    public void setKorak(int korak) {
        this.korak = korak;
    }

    public boolean isNemaKorisnika() {
        return nemaKorisnika;
    }

    public void setNemaKorisnika(boolean nemaKorisnika) {
        this.nemaKorisnika = nemaKorisnika;
    }

    public boolean isSljedeci() {
        return sljedeci;
    }

    public void setSljedeci(boolean sljedeci) {
        this.sljedeci = sljedeci;
    }

    public boolean isPrethodni() {
        return prethodni;
    }

    public void setPrethodni(boolean prethodni) {
        this.prethodni = prethodni;
    }

    private String oblikujPodatke() {
        Gson gson = new Gson();
        com.google.gson.JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ime", imeKorisnika);
        jsonObject.addProperty("prezime", prezimeKorisnika);
        return jsonObject.toString();
    }

    private void napuniListuZaPrikaz(int brojZadnjeg, int brojPocetnog) {
        System.err.println("pocetak: " + brojPocetnog + "kraj: " + brojZadnjeg);
        for (int i = brojPocetnog; i < brojZadnjeg; i++) {
            korisniciZaPrikaz.add(korisnici.get(i));
        }
    }

    public String getPrijavljeniKorisnik() {
        return prijavljeniKorisnik;
    }

    public void setPrijavljeniKorisnik(String prijavljeniKorisnik) {
        this.prijavljeniKorisnik = prijavljeniKorisnik;
    }

    public String getLozinkaPrijavljenog() {
        return lozinkaPrijavljenog;
    }

    public void setLozinkaPrijavljenog(String lozinkaPrijavljenog) {
        this.lozinkaPrijavljenog = lozinkaPrijavljenog;
    }

    public List<Korisnik> getKorisniciZaPrikaz() {
        return korisniciZaPrikaz;
    }

    public void setKorisniciZaPrikaz(List<Korisnik> korisniciZaPrikaz) {
        this.korisniciZaPrikaz = korisniciZaPrikaz;
    }

    private void dohvatiPodatkePrijavljenogKorisnika() {
        for (Korisnik korisnik : korisnici) {
            if (korisnik.getKi().equals(prijavljeniKorisnik)) {
                imeKorisnika = korisnik.getIme();
                prezimeKorisnika = korisnik.getPrezime();
            }

        }
    }

}
