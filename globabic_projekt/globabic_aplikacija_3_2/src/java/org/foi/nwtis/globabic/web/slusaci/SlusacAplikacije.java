package org.foi.nwtis.globabic.web.slusaci;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.konfiguracije.KonfiguracijaApstraktna;
import org.foi.nwtis.globabic.konfiguracije.NeispravnaKonfiguracija;
import org.foi.nwtis.globabic.konfiguracije.NemaKonfiguracije;
import org.foi.nwtis.globabic.konfiguracije.bp.BP_Konfiguracija;

/**
 * Inicijalizira kontekst i pokreće upravljačku dretvu
 *
 * @author Gloria Babić
 */
public class SlusacAplikacije implements ServletContextListener {

    public static ServletContext servletContext;
    public static Konfiguracija konfiguracija = null;
    public static BP_Konfiguracija bP_Konfiguracija = null;
    private String datoteka = "";

    public static ServletContext getServletContext() {
        return servletContext;
    }

    /**
     * Inicijalizira kontekst i pokreće pozadinsku dretvu
     *
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        servletContext = sce.getServletContext();
        String dat = servletContext.getInitParameter("konfiguracija");
        String putanja = servletContext.getRealPath("/WEB-INF") + java.io.File.separator;
        String puniNazivDatoteke = putanja + dat;

        bP_Konfiguracija = new BP_Konfiguracija(puniNazivDatoteke);
        servletContext.setAttribute("BP_Konfig", bP_Konfiguracija);

        try {
            konfiguracija = KonfiguracijaApstraktna.preuzmiKonfiguraciju(puniNazivDatoteke);
            servletContext.setAttribute("konfiguracija", konfiguracija);
            datoteka = konfiguracija.dajPostavku("localFile");

        } catch (NemaKonfiguracije | NeispravnaKonfiguracija ex) {
            Logger.getLogger(SlusacAplikacije.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     *
     * @param sce
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

}
