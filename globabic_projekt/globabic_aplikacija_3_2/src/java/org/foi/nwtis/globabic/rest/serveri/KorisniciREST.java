/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.rest.serveri;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.foi.nwtis.globabic.konfiguracije.Konfiguracija;
import org.foi.nwtis.globabic.web.podaci.Korisnik;
import org.foi.nwtis.globabic.web.slusaci.SlusacAplikacije;

/**
 * REST Web Service
 *
 * @author Gloria Babić
 */
@Path("korisnici")
public class KorisniciREST {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of KorisniciREST
     */
    public KorisniciREST() {
    }

    /**
     * Retrieves representation of an instance of
     * org.foi.nwtis.globabic.rest.serveri.KorisniciREST
     *
     *
     * @param korime
     * @param lozinka
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@HeaderParam("korime") String korime, @HeaderParam("lozinka") String lozinka) {
        String komanda = "KORISNIK " + korime + "; LOZINKA " + lozinka + "; LISTAJ;";
        String stringOdgovor = izvrsiKomadu(komanda);
        JsonObject jsonOdgovor;
        boolean uspjesno = stringOdgovor.contains("OK");

        if (uspjesno) {
            JsonArrayBuilder odgovor = pripremiJsonZaNiz(stringOdgovor);
            jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                    .add("odgovor", odgovor)
                    .add("status", "OK")
                    .build());

        } else {
            jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                    .add("odgovor", "")
                    .add("status", "ERR")
                    .add("poruka", stringOdgovor)
                    .build());
        }

        return jsonOdgovor.toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{korisnickoIme}")
    public String getJson(@PathParam("korisnickoIme") String korisnickoIme, @HeaderParam("korime") String korime, @HeaderParam("lozinka") String lozinka) {

        String komanda = "KORISNIK " + korime + "; LOZINKA " + lozinka + "; PREUZMI " + korisnickoIme + ";";
        String stringOdgovor = izvrsiKomadu(komanda);
        JsonObject jsonOdgovor;
        boolean uspjesno = stringOdgovor.contains("OK");

        if (uspjesno) {
            JsonObject odgovor = pripremiJsonZaJedan(stringOdgovor);
            jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                    .add("odgovor", odgovor)
                    .add("status", "OK")
                    .build());
            System.err.println("ODGOVOR: " + jsonOdgovor.toString());

        } else {
            jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                    .add("odgovor", "")
                    .add("status", "ERR")
                    .add("poruka", stringOdgovor)
                    .build());
        }

        return jsonOdgovor.toString();
    }

    /**
     * PUT method for updating or creating an instance of KorisniciREST
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public String putJson(String content) {

        JsonObject jsonOdgovor;

        jsonOdgovor = (Json.createObjectBuilder()
                .add("odgovor", "")
                .add("status", "ERR")
                .add("poruka", "Nedozvoljena putanja!")
                .build());

        return jsonOdgovor.toString();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String postJson(@HeaderParam("dodaj") String dodaj, @HeaderParam("korime") String korime, @HeaderParam("lozinka") String lozinka, String podaci) {
        JsonObject jsonOdgovor;
        if (dodaj.equals("NE")) {
            jsonOdgovor = postJsonAutentikacija(korime, lozinka);
        } else {
            Gson gson = new Gson();

            Korisnik korisnik = gson.fromJson(podaci, Korisnik.class);
            String komanda = "KORISNIK " + korisnik.getKi() + "; LOZINKA " + korisnik.getLozinka() + "; DODAJ " + korisnik.getIme() + " " + korisnik.getPrezime() + ";";
            String stringOdgovor = izvrsiKomadu(komanda);
            boolean uspjesno = stringOdgovor.contains("OK");

            if (uspjesno) {
                //JsonObject odgovor = pripremiJsonZaJedan(stringOdgovor);
                jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                        .add("odgovor", stringOdgovor)
                        .add("status", "OK")
                        .build());
                System.err.println("ODGOVOR: " + jsonOdgovor.toString());

            } else {
                jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                        .add("odgovor", "")
                        .add("status", "ERR")
                        .add("poruka", stringOdgovor)
                        .build());
            }
        }

        return jsonOdgovor.toString();
    }

    public JsonObject postJsonAutentikacija(String korime, String lozinka) {

        String komanda = "KORISNIK " + korime + "; LOZINKA " + lozinka + ";";
        JsonObject jsonOdgovor;
        String stringOdgovor = izvrsiKomadu(komanda);
        boolean uspjesno = stringOdgovor.contains("OK");

        if (uspjesno) {
            //JsonObject odgovor = pripremiJsonZaJedan(stringOdgovor);
            jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                    .add("odgovor", stringOdgovor)
                    .add("status", "OK")
                    .build());
            System.err.println("ODGOVOR: " + jsonOdgovor.toString());

        } else {
            jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                    .add("odgovor", "")
                    .add("status", "ERR")
                    .add("poruka", stringOdgovor)
                    .build());
        }

        return jsonOdgovor;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{korisnickoIme}")
    public String putJson(@PathParam("korisnickoIme") String korisnickoIme, @HeaderParam("korime") String korime, @HeaderParam("lozinka") String lozinka, String podaci) {
        Gson gson = new Gson();
        JsonObject jsonOdgovor;

        Korisnik korisnik = gson.fromJson(podaci, Korisnik.class);
        String komanda = "KORISNIK " + korisnickoIme + "; LOZINKA " + lozinka + "; AZURIRAJ " + korisnik.getIme() + " " + korisnik.getPrezime() + ";";

        String stringOdgovor = izvrsiKomadu(komanda);
        boolean uspjesno = stringOdgovor.contains("OK");

        if (uspjesno) {
            //JsonObject odgovor = pripremiJsonZaJedan(stringOdgovor);
            jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                    .add("odgovor", stringOdgovor)
                    .add("status", "OK")
                    .build());
            System.err.println("ODGOVOR: " + jsonOdgovor.toString());

        } else {
            jsonOdgovor = (JsonObject) (Json.createObjectBuilder()
                    .add("odgovor", "")
                    .add("status", "ERR")
                    .add("poruka", stringOdgovor)
                    .build());
        }

        return jsonOdgovor.toString();
    }

    public static String izvrsiKomadu(String komanda) {

        ServletContext sc = null;
        Konfiguracija konf;
        sc = SlusacAplikacije.getServletContext();
        konf = (Konfiguracija) sc.getAttribute("konfiguracija");

        String adresa = konf.dajPostavku("adresa");
        System.err.println("adresa" + adresa);
        int port = Integer.parseInt(konf.dajPostavku("port"));
        System.err.println("port" + port);

        String odgovor = "";

        try {
            Socket socket = new Socket(adresa, port);
            InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();
            StringBuffer buffer = new StringBuffer();
            os.write(komanda.getBytes());
            os.flush();
            socket.shutdownOutput();
            while (true) {
                int znak = is.read();
                if (znak == -1) {
                    break;
                }
                buffer.append((char) znak);
            }
            is.close();
            odgovor = buffer.toString();
        } catch (IOException ex) {
            Logger.getLogger(KorisniciREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.err.println("odgovor" + odgovor);

        return odgovor;
    }

    private JsonArrayBuilder pripremiJsonZaNiz(String stringOdgovor) {
        String nizKorisnici = stringOdgovor.substring(stringOdgovor.indexOf(";") + 1);

        Gson gson = new Gson();
        JsonElement element = gson.fromJson(nizKorisnici, JsonElement.class);
        JsonArray odgovor = element.getAsJsonArray();
        JsonArrayBuilder builder = Json.createArrayBuilder();

        for (JsonElement jsonElement : odgovor) {
            Korisnik k = gson.fromJson(jsonElement, Korisnik.class);
            builder.add(Json.createObjectBuilder()
                    .add("ki", k.getKi())
                    .add("prezime", k.getPrezime())
                    .add("ime", k.getIme())
                    .add("email", k.getEmail())
                    .add("vrsta", k.getVrsta()));
        }
        return builder;

    }

    private JsonObject pripremiJsonZaJedan(String stringOdgovor) {
        String korisnik = stringOdgovor.substring(stringOdgovor.indexOf(";") + 1);

        Gson gson = new Gson();
        JsonElement element = gson.fromJson(korisnik, JsonElement.class);
        Korisnik k = gson.fromJson(element, Korisnik.class);

        JsonObject jsonOdgovor;

        jsonOdgovor = (Json.createObjectBuilder()
                .add("ki", k.getKi())
                .add("prezime", k.getPrezime())
                .add("ime", k.getIme())
                .add("email", k.getEmail())
                .add("vrsta", k.getVrsta())
                .build());

        return jsonOdgovor;

    }

}
