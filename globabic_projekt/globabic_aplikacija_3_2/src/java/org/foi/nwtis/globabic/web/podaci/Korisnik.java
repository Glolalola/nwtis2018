/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.web.podaci;

/**
 *
 * @author Gloria Babić
 */
public class Korisnik {

    private String ki;
    private String prezime;
    private String ime;
    private String email;
    private String vrsta;
    private String lozinka;

    public Korisnik() {
    }

    public Korisnik(String ki, String prezime, String ime, String email, String vrsta) {
        this.ki = ki;
        this.prezime = prezime;
        this.ime = ime;
        this.email = email;
        this.vrsta = vrsta;
    }

    public Korisnik(String ki, String prezime, String ime, String email, String vrsta, String lozinka) {
        this.ki = ki;
        this.prezime = prezime;
        this.ime = ime;
        this.email = email;
        this.vrsta = vrsta;
        this.lozinka = lozinka;
    }
    
    
    
    

    public String getKi() {
        return ki;
    }

    public void setKi(String ki) {
        this.ki = ki;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }
    
    
    
    

}
