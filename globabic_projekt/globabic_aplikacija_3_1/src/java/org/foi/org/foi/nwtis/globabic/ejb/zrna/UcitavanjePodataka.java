/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.org.foi.nwtis.globabic.ejb.zrna;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import org.foi.org.foi.nwtis.globabic.ejb.podaci.JMSPoruka;

/**
 *
 * @author Gloria Babić
 */
@Singleton
public class UcitavanjePodataka {
    
    private List<JMSPoruka> poruke = new ArrayList<>();


    public List<JMSPoruka> getPoruke() {
        return poruke;

    }
    
    public boolean dodajPoruku(JMSPoruka poruka) {

        if (poruka != null && !poruke.contains(poruka)) {
            this.poruke.add(poruka);
            return true;
        } else {
            return false;
        }
    }
    
    public void setPoruke(List<JMSPoruka> poruke) {
        System.out.println("Prihvatio poruke");
        this.poruke = poruke;
    }
    
    public boolean obrisiPoruku(JMSPoruka poruka) {
        if (poruka != null && poruke.contains(poruka)) {
            poruke.remove(poruka);
            return true;
        } else {
            return false;
        }
    }
    
     public boolean obrisiPoruke() {
        if (!poruke.isEmpty()) {
            poruke.clear();
            return true;
        } else {
            return false;
        }
    }
     
     
    @PostConstruct
    public void init() {

    }

    @PreDestroy
    public void destroy() {

    }

}
