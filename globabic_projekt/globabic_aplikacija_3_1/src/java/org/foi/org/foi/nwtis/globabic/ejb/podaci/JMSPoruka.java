package org.foi.org.foi.nwtis.globabic.ejb.podaci;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 *
 * @author Gloria Babić
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/NWTiS_globabic_projekt")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class JMSPoruka implements MessageListener {

//    @EJB
//    private UcitavanjePodataka ucitavanjePodataka;

    public JMSPoruka() {
    }


    @Override
    public void onMessage(Message message) {

        ObjectMessage msg = (ObjectMessage) message;
       
        try {
            JMSPoruka mail = (JMSPoruka) msg.getObject();
//            ucitavanjePodataka.dodajPoruku(mail);
//            UredajEndpoint.obavijestiPromjenu("promjena");
        } catch (JMSException ex) {
            Logger.getLogger(JMSPoruka.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
