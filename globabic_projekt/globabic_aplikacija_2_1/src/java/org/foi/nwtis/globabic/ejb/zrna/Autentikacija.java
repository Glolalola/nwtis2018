/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.ejb.zrna;

import javax.ejb.Stateful;
import javax.ejb.LocalBean;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

/**
 *
 * @author Gloria Babić
 */
@Stateful
@LocalBean
public class Autentikacija {

    static class RESTkorisnici_JerseyClient {

        private WebTarget webTarget;
        private Client client;
        private static final String BASE_URI = "http://localhost:8080/globabic_aplikacija_3_2/webresources/";

        public RESTkorisnici_JerseyClient() {
            client = javax.ws.rs.client.ClientBuilder.newClient();
            webTarget = client.target(BASE_URI).path("korisnici");
        }

        /**
         * @param responseType Class representing the response
         * @param requestEntity request data@return response object (instance of responseType class)
         */
        public <T> T putJson(Object requestEntity, Class<T> responseType) throws ClientErrorException {
            return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), responseType);
        }

        /**
         * @param responseType Class representing the response
         * @return response object (instance of responseType class)@param korime header parameter[REQUIRED]
         * @param lozinka header parameter[REQUIRED]
         */
        public <T> T getJson(Class<T> responseType, String korime, String lozinka) throws ClientErrorException {
            return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).header("korime", korime).header("lozinka", lozinka).get(responseType);
        }

        /**
         * @param responseType Class representing the response
         * @param requestEntity request data@return response object (instance of responseType class)@param korime header parameter[REQUIRED]
         * @param lozinka header parameter[REQUIRED]
         */
        public <T> T postJson(Object requestEntity, Class<T> responseType, String korime, String lozinka) throws ClientErrorException {
            return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).header("korime", korime).header("lozinka", lozinka).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), responseType);
        }

        /**
         * @param responseType Class representing the response
         * @return response object (instance of responseType class)@param korime header parameter[REQUIRED]
         * @param lozinka header parameter[REQUIRED]
         */
        public <T> T postJsonAutentikacija(Class<T> responseType, String korime, String lozinka) throws ClientErrorException {
            return webTarget.request().header("korime", korime).header("lozinka", lozinka).post(null, responseType);
        }

        public void close() {
            client.close();
        }
    }
    
    

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
