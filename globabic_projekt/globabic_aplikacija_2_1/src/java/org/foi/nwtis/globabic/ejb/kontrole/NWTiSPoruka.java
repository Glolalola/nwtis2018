/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.ejb.kontrole;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import org.foi.nwtis.globabic.ejb.podaci.Poruka;

/**
 *
 * @author Gloria Babić
 */
public class NWTiSPoruka {

    private String contentType;
    private MimeBodyPart part;
    private String privitak;
    private String porukaID;
    private String sender;
    private String subject;
    private Poruka poruka;
    private int brojPrivitaka;
    private boolean valjanTipSadrzaja;
    private ServletContext sc;
    Poruka.VrstaPoruka vrsta;

    public NWTiSPoruka(String subject) {
        part = new MimeBodyPart();
        brojPrivitaka = 0;
        privitak = "";
        sender = null;
        this.subject = subject;
        poruka = null;
    }

    public NWTiSPoruka() {
    }

    public boolean isNWTiSPoruka(MimeMessage message) throws MessagingException, IOException {
        contentType = message.getContentType().toLowerCase();
        if (message.getSubject().equals(subject)) {
            if (contentType.contains("text/json") || contentType.contains("application/json")) {
                return true;
            }
        }
        return false;
    }
    
        public Poruka citaj(MimeMessage message) throws MessagingException, IOException {

        NWTiSPoruka nwtisporuka = new NWTiSPoruka();
        if (nwtisporuka.isNWTiSPoruka(message)) {
            vrsta = Poruka.VrstaPoruka.NWTiS_poruka;
            privitak = message.getContent().toString();
        } else {
            vrsta = Poruka.VrstaPoruka.neNWTiS_poruka;
        }

        String[] zaglavlje = message.getHeader("Message-ID");
        porukaID = "";
        if (zaglavlje != null && zaglavlje.length > 0) {
            porukaID = zaglavlje[0];
        }
        sender = ((InternetAddress) message.getFrom()[0]).getPersonal();

        if (sender == null) {
            sender = ((InternetAddress) message.getFrom()[0]).getAddress();
        }
        subject = message.getSubject();

        poruka = new Poruka(porukaID, message.getSentDate(), message.getReceivedDate(), sender, subject, privitak, vrsta);

        return poruka;

    }

    public String citajPrilog(String sadrzaj) {

        String regexAdd = "^ADD IoT (\\d{1,6}) \\\"(.*?)\\\" (GPS: ([1-9][0-9]{1,2}.[0-9]{6}),([1-9][0-9]{1,2}.[0-9]{6}));$";
        String regexTemp = "^TEMP IoT (\\d{1,6}) T: ([0-9]{4}).([0-9]{1,3}).([0-9]{1,2}) ([0-2][0-9]:[0-6][0-9]:[0-6][0-9]) C: ([1-9]{1,2}.[0-9])+;$";
        String regexEvent = "EVENT IoT (\\d{1,6}) T: ([0-9]{4}).([0-9]{1,3}).([0-9]{1,2}) ([0-2][0-9]:[0-6][0-9]:[0-6][0-9]) F: ([1-9]{1,2})+;$";

        String[] split = sadrzaj.split(" ");
        Matcher matcher;
        Pattern pattern;
        switch (split[0]) {

            case "ADD":
                pattern = Pattern.compile(regexAdd);
                matcher = pattern.matcher(sadrzaj);
                System.out.println(matcher.matches());
                if (matcher.matches()) {
                    return "uspjeh";
                } else {
                    return "NE";
                }
            case "TEMP":
                pattern = Pattern.compile(regexTemp);
                matcher = pattern.matcher(sadrzaj);
                System.out.println(matcher.matches());
                if (matcher.matches()) {
                    return "uspjeh";
                } else {
                    return "NE";
                }

            case "EVENT":
                pattern = Pattern.compile(regexEvent);
                matcher = pattern.matcher(sadrzaj);
                if (matcher.matches()) {
                    return "uspjeh";
                } else {
                    return "NE";
                }

            default:
                return "NE";

        }

    }

}
