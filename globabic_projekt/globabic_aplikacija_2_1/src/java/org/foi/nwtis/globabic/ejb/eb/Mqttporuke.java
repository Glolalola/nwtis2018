/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.ejb.eb;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gloria Babić
 */
@Entity
@Table(name = "MQTTPORUKE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mqttporuke.findAll", query = "SELECT m FROM Mqttporuke m")
    , @NamedQuery(name = "Mqttporuke.findById", query = "SELECT m FROM Mqttporuke m WHERE m.id = :id")
    , @NamedQuery(name = "Mqttporuke.findByParkiraliste", query = "SELECT m FROM Mqttporuke m WHERE m.parkiraliste = :parkiraliste")
    , @NamedQuery(name = "Mqttporuke.findByVrijeme", query = "SELECT m FROM Mqttporuke m WHERE m.vrijeme = :vrijeme")
    , @NamedQuery(name = "Mqttporuke.findByTekst", query = "SELECT m FROM Mqttporuke m WHERE m.tekst = :tekst")
    , @NamedQuery(name = "Mqttporuke.findByStatus", query = "SELECT m FROM Mqttporuke m WHERE m.status = :status")})
public class Mqttporuke implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PARKIRALISTE")
    private int parkiraliste;
    @Column(name = "VRIJEME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vrijeme;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "TEKST")
    private String tekst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private int status;

    public Mqttporuke() {
    }

    public Mqttporuke(Integer id) {
        this.id = id;
    }

    public Mqttporuke(Integer id, int parkiraliste, String tekst, int status) {
        this.id = id;
        this.parkiraliste = parkiraliste;
        this.tekst = tekst;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getParkiraliste() {
        return parkiraliste;
    }

    public void setParkiraliste(int parkiraliste) {
        this.parkiraliste = parkiraliste;
    }

    public Date getVrijeme() {
        return vrijeme;
    }

    public void setVrijeme(Date vrijeme) {
        this.vrijeme = vrijeme;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mqttporuke)) {
            return false;
        }
        Mqttporuke other = (Mqttporuke) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.foi.nwtis.globabic.ejb.eb.Mqttporuke[ id=" + id + " ]";
    }
    
}
