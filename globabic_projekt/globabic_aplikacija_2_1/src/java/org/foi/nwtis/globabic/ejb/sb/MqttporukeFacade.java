/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.ejb.sb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.foi.nwtis.globabic.ejb.eb.Mqttporuke;

/**
 *
 * @author Gloria Babić
 */
@Stateless
public class MqttporukeFacade extends AbstractFacade<Mqttporuke> {

    @PersistenceContext(unitName = "globabic_aplikacija_2_1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MqttporukeFacade() {
        super(Mqttporuke.class);
    }
    
}
