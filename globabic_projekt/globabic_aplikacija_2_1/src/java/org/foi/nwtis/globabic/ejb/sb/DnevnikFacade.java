/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.ejb.sb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.foi.nwtis.globabic.ejb.eb.Dnevnik;
import org.foi.nwtis.globabic.ejb.eb.Dnevnik_;

/**
 *
 * @author Gloria Babić
 */
@Stateless
public class DnevnikFacade extends AbstractFacade<Dnevnik> {

    @PersistenceContext(unitName = "globabic_aplikacija_2_1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DnevnikFacade() {
        super(Dnevnik.class);
    }

    public List<Dnevnik> filtrirajDnevnike(String korisnik, String ipadresa, Date odVremena, Date doVremena, String trajanje, String adresaZahtjeva) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Dnevnik> cq = cb.createQuery(Dnevnik.class);
        Root<Dnevnik> e = cq.from(Dnevnik.class);

        List<Predicate> predicates = new ArrayList<Predicate>();
        if (korisnik != null && !korisnik.isEmpty()) {
            predicates.add(cb.equal(e.get("korisnik"), korisnik));
        }

        if (ipadresa != null && !ipadresa.isEmpty()) {
            predicates.add(cb.equal(e.get("ipadresa"), ipadresa));
        }

        if (trajanje != null && !trajanje.equals("")) {
            predicates.add(cb.equal(e.get("trajanje"), trajanje));
        }
        if (adresaZahtjeva != null && !adresaZahtjeva.equals("")) {
            predicates.add(cb.equal(e.get("url"), adresaZahtjeva));
        }

        if (odVremena != null) {
            predicates.add(cb.greaterThan(e.get("vrijeme").as(Date.class), odVremena));
        }

        if (doVremena != null) {
            predicates.add(cb.lessThan(e.get("vrijeme").as(Date.class), doVremena));
        }

        if (!predicates.isEmpty()) {
            cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        }

        List<Dnevnik> dnevnici = em.createQuery(cq).getResultList();
        return dnevnici;
    }

}
