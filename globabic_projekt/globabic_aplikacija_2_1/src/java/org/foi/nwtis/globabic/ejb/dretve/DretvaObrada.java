/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.ejb.dretve;

import com.google.gson.Gson;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;
import org.foi.nwtis.globabic.ejb.kontrole.NWTiSPoruka;
import org.foi.nwtis.globabic.ejb.podaci.Komanda;
import org.foi.nwtis.globabic.ejb.podaci.Poruka;

/**
 *
 * @author Gloria Babić
 */
public class DretvaObrada extends Thread {


    private String lozinka;
    private String posluzitelj;
    private String korime;
    private int spavanje;
    private boolean radi = true;
    private Message[] messages = null;
    private int brojPorukaZaDohvatiti;
    private String nwtisMapa;
    private int port;
    private String predmet;

    private SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy:HH:mm:SS Z");

    private int obradaPorukaBroj;
    private long vrijemePocetka;
    private long vrijemeKraja;
    private String obradaZapocela;// = DATE_FORMAT.format(today);
    private String obradaZavrsila;
    private double trajanjeObradems;
    private int brojPoruka;
    private int brojNwtisPoruka;
    private int brojProcitanih;

    public DretvaObrada(String lozinka, String posluzitelj, String korime, int spavanje, int brojPorukaZaDohvatiti, String nwtisMapa, int port, String predmet) {
        this.lozinka = lozinka;
        this.posluzitelj = posluzitelj;
        this.korime = korime;
        this.spavanje = spavanje;
        this.brojPorukaZaDohvatiti = brojPorukaZaDohvatiti;
        this.nwtisMapa = nwtisMapa;
        this.port = port;
        this.predmet = predmet;
    }

    @Override
    public void interrupt() {
        radi = false;
        super.interrupt(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void run() {
        super.run();
        obradaPorukaBroj = 0;
        vrijemePocetka = System.currentTimeMillis();
        obradaZapocela = DATE_FORMAT.format(vrijemePocetka);

        while (radi) {

            try {
                Session session;
                Store store;
                Folder folder;

                java.util.Properties properties = System.getProperties();
                properties.put("mail.smtp.host", posluzitelj);
                session = Session.getInstance(properties, null);

                store = session.getStore("imap");
                store.connect(posluzitelj, korime, lozinka);

                folder = store.getFolder("INBOX");
                folder.open(Folder.READ_ONLY);

                Folder newfolder = store.getFolder(nwtisMapa);
                if (!newfolder.exists()) {
                    newfolder.create(Folder.HOLDS_MESSAGES);
                }

                messages = folder.getMessages();
                for (int i = 0; i < messages.length; ++i) {
                    MimeMessage message = (MimeMessage) messages[i];
                    NWTiSPoruka nWTiSPoruka = new NWTiSPoruka(predmet);
                    Poruka poruka = null;
                    poruka = nWTiSPoruka.citaj(message);
                    boolean nwtis = nWTiSPoruka.isNWTiSPoruka(message);
                    if (nwtis) {
                        boolean obradena = ObradaNwtisPoruke(poruka);
                        if (obradena) {
                            System.out.println("OBRAĐUJE: " + poruka);
                            Message[] poruke = new Message[1];
                            poruke[0] = message;
                            folder.copyMessages(poruke, newfolder);
                            message.setFlag(Flags.Flag.DELETED, true);
                            folder.expunge();
                        }
                    }

                }

                folder.close(false);
                store.close();

                vrijemeKraja = System.currentTimeMillis();
                obradaZavrsila = DATE_FORMAT.format(vrijemeKraja);
                System.out.println("Završila iteracija: " + (obradaPorukaBroj++) + "!");

                sleep(spavanje * 1000);

            } catch (NoSuchProviderException ex) {
                Logger.getLogger(DretvaObrada.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MessagingException ex) {
                Logger.getLogger(DretvaObrada.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(DretvaObrada.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DretvaObrada.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public synchronized void start() {
        super.start();
    }

    private boolean ObradaNwtisPoruke(Poruka poruka) {

        String tekstPrivitka = poruka.getPrivitak();
        System.out.println("TEKST PRIVITKA: " + tekstPrivitka);
        if (provjeriKomandu(tekstPrivitka)) {
            Gson gson = new Gson();
            Komanda komanda = gson.fromJson(tekstPrivitka, Komanda.class);
            
        }
        return false;
    }

    public boolean provjeriKomandu(String komanda) {
        Pattern valjanaKomandaRegex = Pattern.compile("\\{(\"id\"): ?(\\d{1,4}), ?(\"komanda\"): ?(\"dodaj\"|\"azuriraj\"), ?((?:\"[a-zA-Z ]{1,30}\": ?(?:[0-9]{1,3}|[0-9]{1,3}\\.[0-9]{1,2}|\"[a-zA-Z ]{1,30}\"),\\s?){1,5}) ?(\"vrijeme\"): ?\"([0-9]{4}\\.[0-9]{2}\\.[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}){1,3}\"\\}");
        Matcher matcher = valjanaKomandaRegex.matcher(komanda);
        boolean vrati = matcher.find();
        return vrati;
    }



}
