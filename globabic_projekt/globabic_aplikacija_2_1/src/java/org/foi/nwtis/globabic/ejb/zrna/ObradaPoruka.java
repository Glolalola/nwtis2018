/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.foi.nwtis.globabic.ejb.zrna;

import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Startup;

/**
 *
 * @author Gloria Babić
 */
@Singleton
@LocalBean
@Startup
public class ObradaPoruka {
    
    String server;
    int port;
    String korisnik;
    String lozinka;
    int brojPrikazanihPoruka;
    int trajanjeCiklusa;
    String predmet;
    String folderNWTiS;
    String folderOther;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(String korisnik) {
        this.korisnik = korisnik;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public int getBrojPrikazanihPoruka() {
        return brojPrikazanihPoruka;
    }

    public void setBrojPrikazanihPoruka(int brojPrikazanihPoruka) {
        this.brojPrikazanihPoruka = brojPrikazanihPoruka;
    }

    public int getTrajanjeCiklusa() {
        return trajanjeCiklusa;
    }

    public void setTrajanjeCiklusa(int trajanjeCiklusa) {
        this.trajanjeCiklusa = trajanjeCiklusa;
    }

    public String getPredmet() {
        return predmet;
    }

    public void setPredmet(String predmet) {
        this.predmet = predmet;
    }

    public String getFolderNWTiS() {
        return folderNWTiS;
    }

    public void setFolderNWTiS(String folderNWTiS) {
        this.folderNWTiS = folderNWTiS;
    }

    public String getFolderOther() {
        return folderOther;
    }

    public void setFolderOther(String folderOther) {
        this.folderOther = folderOther;
    }
    
    
}
